#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define MAX_LL 9223372036854775807
#define MIN_LL -9223372036854775807
struct Node{
	Node *lTree, *rTree;
	long long int l, r, sum, mn, mx, tag, len;
};
inline void buildTree(Node* root, int l, int r){
	root -> lTree = nullptr;
	root -> rTree = nullptr;
	root -> l = l;
	root -> r = r;
	root -> sum = 0;
	root -> mn = 0;
	root -> mx = 0;
	root -> tag = 0;
	root -> len = r - l + 1;
	if(l == r){
		root -> lTree = nullptr;
		root -> rTree = nullptr;
		return;
	}
	else{
		int mid = (l + r) / 2;
		root -> lTree = new Node;
		root -> rTree = new Node;
		buildTree(root -> lTree, l, mid);
		buildTree(root -> rTree, mid+1, r);
	}
	return;
}
inline void push(Node* &root){
	if(root != nullptr && root -> l != root -> r){
		if(root -> lTree != nullptr){
			root -> lTree -> tag += root -> tag;
			root -> lTree -> mn  += root -> tag;
			root -> lTree -> mx  += root -> tag;
			root -> lTree -> sum += (root -> tag) * (root -> lTree -> len);
		}
		if(root -> rTree != nullptr){
			root -> rTree -> tag += root -> tag;
			root -> rTree -> mn  += root -> tag;
			root -> rTree -> mx  += root -> tag;
			root -> rTree -> sum += (root -> tag) * (root -> rTree -> len);
		}
		root -> tag = 0;
		return;
	}
	else return;
}
inline void pull(Node* &root){
	push(root);
	if(root != nullptr && root -> l != root -> r){
		root -> mn  = min(root -> lTree -> mn, root -> rTree -> mn);
		root -> mx  = max(root -> lTree -> mx, root -> rTree -> mx);
		root -> sum = root -> lTree -> sum + root -> rTree -> sum;	 
		return;
	}
	else return;
}
inline long long int queryMin(Node* root, const long long int &l, const long long int &r){
	if(root == nullptr){
		return MAX_LL;
	}
	//push(root);
	//pull(root);
	if(root -> l >= l && root -> r <= r){
		return root -> mn;
	}
	else if(root -> r < l || root -> l > r){
		return MAX_LL;
	}
	else{
		return min(queryMin(root -> lTree, l, r), queryMin(root -> rTree, l, r));
	}
}
inline long long int queryMax(Node* root, const long long int &l, const long long int &r){
	if(root == nullptr){
		return MIN_LL;
	}
	//push(root);
	//pull(root);
	if(root -> l >= l && root -> r <= r){
		return root -> mx;
	}
	else if(root -> r < l || root -> l > r){
		return MIN_LL;
	}
	else{
		return max(queryMax(root -> lTree, l, r), queryMax(root -> rTree, l, r));
	}
}
inline long long int querySum(Node* root, const long long int &l, const long long int &r){
	if(root == nullptr){
		return 0;
	}
	//push(root);
	//pull(root);
	if(root -> l >= l && root -> r <= r){
		return root -> sum;
	}
	else if(root -> r < l || root -> l > r){
		return 0;
	}
	else{
		return querySum(root -> lTree, l, r) + querySum(root -> rTree, l, r);
	}
}
inline void update(Node* root, const long long int &l, const long long int &r, const long long int &addVal){
	push(root);
	if(root -> l >= l && root -> r <= r){
		root -> tag += addVal;
		root -> mn  += addVal;
		root -> mx  += addVal;
		root -> sum += (root -> len) * (addVal);
	}
	else if(root -> r < l || root -> l > r){
		return;
	}
	else{
		update(root -> lTree, l, r, addVal);
		update(root -> rTree, l, r, addVal);
	}
	pull(root);
	return;
}
inline void pt(Node* root){
	push(root);
	pull(root);
	if(root == nullptr) return;
	cout << "rootL: " << root -> l << " rootR: " << root -> r << " rootSUM: " << root -> sum << " rootMX: " << root -> mx << " rootMN: " << root -> mn << " rootTAG: " << root -> tag << endl;
	pt(root -> lTree);
	pt(root -> rTree);
}
inline void clean(Node* root){
	if(root == nullptr) return;
	if(root -> l == root -> r){
		delete root;
		return;
	}
	clean(root -> lTree);
	clean(root -> rTree);
	delete root;
}
int main(){
	cin.tie(0); ios::sync_with_stdio(0);
	Node* root = new Node;
	clean(root);
}