#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;
#define MAX_INT 2147483647
#define MIN_INT -2147483647
class Node{
public:
	long long int idx, l, r, sum, mn, mx, tag, len;
	Node(){
		idx = 0;
		l = 0;
		r = 0;
		sum = 0;
		mn = 0;
		mx = 0;
		tag = 0;
		len = 0;
	}
};
vector<Node> segTree(500000 << 2);
inline void push(const long long int &idx){
	if(segTree[idx].l != segTree[idx].r){
		long long int lTree = idx*2;
		long long int rTree = idx*2+1;
		segTree[lTree].tag += segTree[idx].tag;
		segTree[lTree].mn  += segTree[idx].mn;
		segTree[lTree].mx  += segTree[idx].mx;
		segTree[lTree].sum += segTree[idx].sum;
		segTree[rTree].tag += segTree[idx].tag;
		segTree[rTree].mn  += segTree[idx].mn;
		segTree[rTree].mx  += segTree[idx].mx;
		segTree[rTree].sum += segTree[idx].sum;
	}
	segTree[idx].tag = 0;
	return;
}
inline void pull(const long long int &idx){
	push(idx);
	if(segTree[idx].l != segTree[idx].r){
		long long int lTree = idx*2;
		long long int rTree = idx*2+1;
		segTree[idx].mn = min(segTree[lTree].mn, segTree[rTree].mn);
		segTree[idx].mx = min(segTree[lTree].mx, segTree[rTree].mx);
		segTree[idx].sum = segTree[lTree].sum + segTree[rTree].sum;
	}
	return;
}
inline long long int queryMax(const long long int &idx, const long long int &l, const long long int &r){
	long long int lTree = idx*2;
	long long int rTree = idx*2+1;
	if(segTree[idx].l >= l && segTree[idx].r <= r){
		return segTree[idx].mx;
	}
	else if(segTree[idx].r < l || segTree[idx].l > r){
		return MIN_INT;
	}
	else{
		return max(queryMax(lTree, l, r), queryMax(rTree, l, r));
	}
}
inline void update(const long long int &idx, const long long int &l, const long long int &r, const long long int &addVal){
	push(idx);
	if(segTree[idx].l >= l && segTree[idx].r <= r){
		segTree[idx].tag += addVal;
		segTree[idx].mn  += addVal;
		segTree[idx].mx  += addVal;
		segTree[idx].sum += addVal;
	}
	else if(segTree[idx].r < l || segTree[idx].l > r){
		return;
	}
	else{
		long long int lTree = idx*2;
		long long int rTree = idx*2+1;
		update(lTree, l, r, addVal);
		update(rTree, l, r, addVal);
	}
	pull(idx);
	return;
}
inline void buildTree(long long int idx, long long int l, long long int r){
	segTree[idx].l = l;
	segTree[idx].r = r;
	segTree[idx].sum = 0;
	segTree[idx].mn = 0;
	segTree[idx].mx = 0;
	segTree[idx].tag = 0;
	segTree[idx].len = r - l + 1;
	if(l == r){
		return;
	}
	else{
		long long int mid = (l+r) / 2;
		long long int lTree = idx*2;
		long long int rTree = idx*2+1;
		buildTree(lTree, l, mid);
		buildTree(rTree, mid+1, r);
	}
	return;
}
int main(){
	cin.tie(0); ios::sync_with_stdio(0);
	long long int n;
	cin >> n;
	buildTree(1, 1, n);
	for(long long int i = 1; i <= n; i++){
		long long int val;
		cin >> val;
		update(1, i, i, val);
	}
	push(1);
	pull(1);
	for(int i = 0; i < 11; i++){
		cout << segTree[i].mx << endl;
	}
	long long int q;
	cin >> q;
	while(q--){
		long long int l, r;
		cin >> l >> r;
		cout << queryMax(1, min(l, r), max(l, r)) << endl;
	}
}