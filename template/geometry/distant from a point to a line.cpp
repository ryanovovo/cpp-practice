#include<bits/stdc++.h>
using namespace std;
typedef double Double;
struct Point {
  	Double x,y;
  	bool operator < (const Point &b)const{    // 比較
    	return tie(x,y) < tie(b.x,b.y);
    	//return atan2(y,x) < atan2(b.y,b.x);
  	}
  	Point operator + (const Point &b)const{   // 向量加法
    	return {x+b.x,y+b.y};
  	}
  	Point operator - (const Point &b)const{   // 向量減法
    	return {x-b.x,y-b.y};
  	}
  	Point operator * (const Double &d)const{  // 向量伸縮
    	return {d*x, d*y};
  	}
  	Double operator * (const Point &b)const{  // 向量內積
    	return x*b.x + y*b.y;
  	}
  	Double operator % (const Point &b)const{  // 向量外積
    	return x*b.y - y*b.x;
  	}
  	Double abs(){
  		return sqrt(x*x + y*y);
  	}
  	Double abs2(){
  		return (x*x + y*y);
  	}
};
double dist_point_to_line(point &u, point &v){
	return abs((u%v)/abs(v));
}
int main(){}