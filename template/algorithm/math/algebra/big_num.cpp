#include<bits/stdc++.h>
using namespace std;
class big_num{
public:
	deque<int> num;
	big_num();
	~big_num();
	int length();
	int operator [](const int& idx);
};
big_num(string n){//input number to construct a big number
	big_num.num.clear();
	for(auto it : n){
		int val = it - '0';
		big_num.num.push_back(val);
	}
}
~big_num(){
	big_num.num.clear();
}

int length{
	return big_num.num.size();
}

int operator [](const int& idx){
	return big_num.num[idx];
}

bool operator >(const big_num& lhs, const big_num rhs){
	if(lhs.length() > rhs.length()){
		return true;
	}
	else if(rhs.length() > lhs.length()){
		return false;
	}
	for(int i = 0; i < lhs.length(); i++) {
		if(lhs[i] > rhs[i]){
			return true;
		}
		else if(lhs[i] < rhs[i]){
			return false;
		}
	}
	return false;
}

bool operator < (const big_num& lhs, const big_num rhs){
	if(lhs.length() < rhs.length()){
		return true;
	}
	else if(lhs.length() > rhs.length()){
		return false;
	}
	for(int i = 0; i < lhs.length; i++) {
		if(lhs[i] < rhs[i]){
			return true;
		}
		else if(lhs[i] > rhs[i]){
			return false;
		}
	}
	return false;
}
big_num operator +(const big_num& augend, big_num& addend){
	if(augend < addend){
		return (addend + augend);
	}
	

}

int main(){

}