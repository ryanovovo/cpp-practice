#include<iostream>
#include<vector>
#include<cstdio>
#include<algorithm>
#include<sstream>
using namespace std;
int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	string stra, strb;
	cin >> stra;
	cin >> strb;
	//reverse(stra.begin(), stra.end());
	//reverse(strb.begin(), strb.end());
	long long int a = 0;
	long long int b = 0;
	long long int cnta = 1;
	long long int cntb = 1;
	//cout << stra << strb << endl;
	for(auto it : stra){
		if(it == '[' || it == ']' || it == ','){
			continue;
		}
		else{
			int tmpnum = it - '0';
			a+= tmpnum*cnta;
			cnta*=10;
			//cout << a << endl;
		}
	}
	for(auto it : strb){
		if(it == '[' || it == ']' || it == ','){
			continue;
		}
		else{
			int tmpnum = it - '0';
			b+= tmpnum*cntb;
			cntb*=10;
			//cout << b << endl;
		}
	}
	//cout << a << "," << b << endl;
	long long int ans = a+b;
	cout << '[';
	while(ans != 0){
		cout << ans%10;
		if(ans /10 != 0){
			cout << ", ";
			ans /= 10;
		}
		else{
			break;
		}
	}
	cout << ']';
	cout << endl;
}