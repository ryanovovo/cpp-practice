#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
vector<vector<int>> graph(5, vector<int>(5, 0));
void bfs(pair<int, int> cd){
	int x = cd.first;
	int y = cd.second;
	if(x < 0 || x >=5 || y < 0 || y >= 5){
		return;
	}
	else if(graph[x][y] == 0){
		return;
	}
	else{
		//cout << x << " " << y << endl;
		graph[x][y] = 0;
		bfs({x+1, y});
		bfs({x-1, y});
		bfs({x, y+1});
		bfs({x, y-1});
	}
	return;
}
int main(){
	for(int j = 0; j < 5; j++){
		for(int i = 0; i < 5; i++){
			cin >> graph[i][j];
		}
	}
	int ans = 0;
	for(int i = 0; i < 5; i++){
		for(int j = 0; j < 5; j++){
			if(graph[i][j] == 1){
				bfs({i, j});
				ans++;
			}
		}
	}
	cout << ans << endl;
}