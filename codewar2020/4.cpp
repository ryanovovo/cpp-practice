#include<iostream>
#include<vector>
using namespace std;
int main(){
	vector<int> arr;
	int tmp;
	while(cin >> tmp){
		arr.push_back(tmp);
	}
	int ans = 1;
	if(arr.size() == 1 && arr[0] == 0){
		cout << "True" << endl;
		return 0;
	}
	for(int it : arr){
		ans--;
		if(ans < 0){
			cout << "False" << endl;
			return 0;
		}
		ans = max(ans, it);
	}
	cout << "True" << endl;
}
