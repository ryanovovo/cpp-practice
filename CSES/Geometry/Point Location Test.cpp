#include <bits/stdc++.h>
using namespace std;

template <typename T, typename U>
pair<T, U> operator+(const pair<T, U> &l, const pair<T, U> &r){
	return {l.first+r.first, l.second+r.second};
}

template <typename T, typename U>
pair<T, U> operator-(const pair<T, U> &l, const pair<T, U> &r){
	return {l.first-r.first, l.second-r.second};
}

template <typename T, typename U>
T dot(const pair<T, U> &l, const pair<T, U> &r){
	return l.first*r.first+l.second*r.second;
}

template <typename T, typename U>
T cross(const pair<T, U> &l, const pair<T, U> &r){
	return l.first*r.second-l.second*r.first;
}


int main(){
	int t;
	cin >> t;
	while(t--){
		vector<pair<long long int, long long int>> p(3);
		for(int i = 0; i < 3; i++){
			cin >> p[i].first >> p[i].second;
		}
		pair<long long int, long long int> v = p[1]-p[0];
		pair<long long int, long long int> u = p[2]-p[0];
		long long int cp = cross(v, u);
		if(cp > 0){
			cout << "LEFT" << endl;
		}
		else if(cp == 0){
			cout << "TOUCH" << endl; 
		}
		else if(cp < 0){
			cout << "RIGHT" << endl;
		}
	}
}