#include <bits/stdc++.h>
using namespace std;

template <typename T, typename U>
pair<T, U> operator+(const pair<T, U> &l, const pair<T, U> &r){
	return {l.first+r.first, l.second+r.second};
}

template <typename T, typename U>
pair<T, U> operator-(const pair<T, U> &l, const pair<T, U> &r){
	return {l.first-r.first, l.second-r.second};
}

template <typename T, typename U>
T dot(const pair<T, U> &l, const pair<T, U> &r){
	return l.first*r.first+l.second*r.second;
}

template <typename T, typename U>
T cross(const pair<T, U> &l, const pair<T, U> &r){
	return l.first*r.second-l.second*r.first;
}

int main(){
	int n;
	cin >> n;
	vector<pair<long long int, long long int>> v(n);
	for(int i = 0; i < n; i++){
		cin >> v[i].first >> v[i].second;
	}
	long long int ans = 0;
	for(int i = 0; i < n; i++){
		ans += cross(v[i], v[(i+1)%n]);
	}
	cout << abs(ans) << endl;
}