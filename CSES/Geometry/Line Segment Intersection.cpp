#include <bits/stdc++.h>
using namespace std;

template <typename T, typename U>
pair<T, U> operator+(const pair<T, U> &l, const pair<T, U> &r){
	return {l.first+r.first, l.second+r.second};
}

template <typename T, typename U>
pair<T, U> operator-(const pair<T, U> &l, const pair<T, U> &r){
	return {l.first-r.first, l.second-r.second};
}

template <typename T, typename U>
T dot(const pair<T, U> &l, const pair<T, U> &r){
	return l.first*r.first+l.second*r.second;
}

template <typename T, typename U>
T cross(const pair<T, U> &l, const pair<T, U> &r){
	return l.first*r.second-l.second*r.first;
}

template <typename T>
bool samesign(const T &r, const T &l){
	if((r > 0 && l > 0) || (r < 0 && l < 0)){
		return true;
	}
	else{
		return false;
	}
}

template <typename T, typename U>
bool intersection(const pair<T, U> &a, const pair<T, U> &b, const pair<T, U> &c, const pair<T, U> d){

	pair<T, U> Vab = b - a;
	pair<T, U> Vba = a - b;
	pair<T, U> Vcd = d - c;
	pair<T, U> Vdc = c - d;
	pair<T, U> Vac = c - a;
	pair<T, U> Vad = d - a;
	pair<T, U> Vda = a - d;
	pair<T, U> Vca = a - c;
	pair<T, U> Vcb = b - c;
	pair<T, U> Vbc = c - b;
	pair<T, U> Vbd = d - b;
	pair<T, U> Vdb = b - d;

	if((cross(Vab, Vac) == 0 && cross(Vab, Vad) == 0) || (cross(Vcd, Vca) == 0 && cross(Vcd, Vcb) == 0)){
		if((dot(Vab, Vac) >= 0 && dot(Vbc, Vba) >= 0) || (dot(Vab, Vad) >= 0 && dot(Vbd, Vba) >= 0)){
			return true;
		}
		if((dot(Vcd, Vca) >= 0 && dot(Vda, Vdc) >= 0) || (dot(Vcd, Vcb) >= 0 && dot(Vdb, Vdc) >= 0)){
			return true;
		}

		else{
			return false;
		}
	}
	else if(!samesign(cross(Vab, Vac), cross(Vab, Vad)) && !samesign(cross(Vcd, Vca), cross(Vcd, Vcb))){
		return true;
	}
	else{
		return false;
	}
	
}


int main(){
	int t;
	cin >> t;
	while(t--){
		vector<pair<long long int, long long int>> p(4);
		for(int i = 0; i < 4; i++){
			cin >> p[i].first >> p[i].second;
		}
		if(intersection(p[0], p[1], p[2], p[3])){
			cout << "YES" << endl;
		}
		else{
			cout << "NO" << endl;
		}
	}
}
