#include<bits/stdc++.h>
using namespace std;
long long int exp(long long int a, long long int b){
	long long int res = 1;

	while(b != 0){
		if(b % 2 == 1){
			res = (res%1000000007)*(a % 1000000007);
			res = res % 1000000007;
			a = a %1000000007;
		}
		a = ((a%1000000007)*(a%1000000007))%1000000007;
		b/=2;
	}
	return res;
}
int main(){
	int n;
	cin >> n;
	while(n--){
		long long int a, b;
		cin >> a >> b;
		cout << exp(a, b) << endl;
	}
}