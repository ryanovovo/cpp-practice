#include<bits/stdc++.h>
using namespace std;
vector<int> v(1000001, 0);
int solve(int x){
	vector<int> cnt;
	while(x != 1){
		int m = v[x];
		int c = 0;
		while(x % m == 0){
			c++;
			x /= m;
		}
		cnt.push_back(c);
	}
	int res = 1;
	for(auto it : cnt){
		res = res*(it+1);
	}
	return res;
}
int main(){
	int n;
	cin >> n;
	for(int i = 2; i <= 1000000; i++){
		if(v[i] == 0){
			for(int j = i; j <= 1000000; j+=i){
				if(v[j] == 0){
					v[j] = i;
				}
			}
		}
	}
	while(n--){
		int x;
		cin >> x;
		cout << solve(x) << endl;
	}
}