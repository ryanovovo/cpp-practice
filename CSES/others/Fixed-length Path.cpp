#include<bits/stdc++.h>
using namespace std;

void dfs(int root, vector<vector<int>> &tree, vector<int> &cnt, int par, int dep){
	cnt[dep]++;
	for(auto des : tree[root]){
		if(des == par){
			continue;
		}
		dfs(des, tree, cnt, root, dep+1);
	}
	return;
}

int k;
int solve(int root, vector<vector<int>> &tree,vector<int> &cnt, vector<int> &t){
	cnt[0] = 1;
	int ans = 0;
	for(auto des : tree[root]){
		dfs(des, tree, t, root, 1);
		for(int i = 1; t[i] > 0; i++){
			if(k - i >= 0){
				ans += t[i] * cnt[k-i];
			}
		}
		for(int i = 1; t[i] > 0; i++){
			cnt[i] += t[i];
			t[i] = 0;
		}
	}
	return ans;
}

TabNine::config

int main(){
	int n;
	cin >> n >> k;
	vector<vector<int>> tree(n+1);
	vector<int> cnt(n+1);
	vector<int> t(n+1);
	for(int i = 0; i < n-1; i++){
		int a, b;
		cin >> a >> b;
		tree[a].push_back(b);
		tree[b].push_back(a);
	}
	cout << solve(1, tree, cnt, t) << endl;
}