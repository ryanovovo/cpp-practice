#include<bits/stdc++.h>
using namespace std;

int main(){
	int n;
	cin >> n;
	vector<vector<long long int>> dir_dec(n+1);
	vector<vector<long long int>> dir_anc(n+1);
	for(int i = 2; i <= n; i++){
		int boss;
		cin >> boss;
		dir_dec[boss].push_back(i);
		dir_anc[i].push_back(boss);
	}
	vector<long long int> cnt(n+1, 1);
	queue<long long int> q;
	vector<int> is_done(n+1, 0);
	for(int i = 2; i <= n; i++){
		if(dir_dec[i].size() == 0){
			q.push(i);
			is_done[i]++;
		}
	}
	
	while(!q.empty()){
		int l = q.front();
		for(auto anc : dir_anc[l]){
			cnt[anc] += cnt[l];
			is_done[anc]++;
			if(dir_dec[anc].size() == is_done[anc]){
				q.push(anc);
			}
		}
		q.pop();
		//s.erase(l);
	}
	for(int i = 1; i <= n; i++){
		cout << cnt[i]-1 << ' ';
	}
	cout << endl;
}