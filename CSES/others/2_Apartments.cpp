#include<bits/stdc++.h>
using namespace std;
int main(){
	long long int n, m, k;
	cin >> n >> m >> k;
	vector<long long int> a(n);
	vector<long long int> b(m);
	for(long long int i = 0; i < n; i++){
		cin >> a[i];
	}
	for(long long int i = 0; i < m; i++){
		cin >> b[i];
	}
	long long int i = 0;
	long long int j = 0;
	long long int ans = 0;
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());
	while(i < n && j < m){
		if(abs(a[i]-b[j]) <= k){
			i++;
			j++;
			ans++;
		}
		else{
			if(a[i] > b[j]){
				j++;
			}
			else{
				i++;
			}
		}
	}
	cout << ans << endl;
}