#include <bits/stdc++.h>
using namespace std;
void dfs(int root, vector<vector<int>> &tree, vector<int> &anc, vector<vector<int>> &des, vector<int> &is_traveled){
	is_traveled[root] = 1;
	for(auto d : tree[root]){
		if(is_traveled[d] == 0){
			des[root].push_back(d);
			anc[d] = root;
			dfs(d, tree, anc, des, is_traveled);
		}
	}
	return;
}

int main(){
	int n;
	cin >> n;
	vector<int> anc(n+1);
	vector<vector<int>> des(n+1);
	vector<pair<int, int>> depth(n+1, {0, 0});
	vector<vector<int>> tree(n+1);
	vector<int> is_finished(n+1, 0);
	vector<int> is_traveled(n+1, 0);
	for(int i = 0; i < n - 1; i++){
		int a, b;
		cin >> a >> b;
		tree[a].push_back(b);
		tree[b].push_back(a);
	}
	dfs(1, tree, anc, des, is_traveled);
	queue<int> q;
	for(int i = 1;i <= n; i++){
		if(des[i].size() == 0){
			q.push(i);
		}
	}
	while(!q.empty()){
		int l = q.front();
		//cout << "l: " << l <<" des[l].size() " << des[l].size() << endl;
		//cout << endl;
		if(des[l].size() == is_finished[l] && anc[l] != 0){
			if(depth[anc[l]].first < depth[l].first + 1){
				swap(depth[anc[l]].first, depth[anc[l]].second);
				depth[anc[l]].first = depth[l].first + 1;
			}
			else if(depth[anc[l]].first >= depth[l].first + 1 && depth[l].first + 1 > depth[anc[l]].second){
				depth[anc[l]].second = depth[l].first + 1;
			}
			is_finished[anc[l]]++;
		}
		if(anc[l] != 0 && is_finished[anc[l]] == des[anc[l]].size()){
			q.push(anc[l]);
		}
		q.pop();
	}
	/*
	for(int i = 1; i <= n; i++){
		cout << i << ": " << depth[i].first << " " << depth[i].second << ' ';
	}
	cout << endl;
	*/
	int ans = 0;
	for(auto dep : depth){
		ans = max(ans, dep.first + dep.second);
	}
	cout << ans << endl;

}