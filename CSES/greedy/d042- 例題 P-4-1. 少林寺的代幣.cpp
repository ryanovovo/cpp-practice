#include<bits/stdc++.h>
using namespace std;
int main(){
	int n;
	cin >> n;
	while(n--){
		int m;
		cin >> m;
		int ans = 0;
		while(m > 0){
			if(m >= 50){
				m -= 50;
			}
			else if(m >= 10){
				m -= 10;
			}
			else if(m >= 5){
				m -= 5;
			}
			else if(m >= 1){
				m -= 1;
			}
			ans++;
		}
		cout << ans << endl;
	}
}