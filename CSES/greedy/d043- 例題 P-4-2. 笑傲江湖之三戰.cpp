#include<bits/stdc++.h>
using namespace std;
int main(){
	int n;
	cin >> n;
	vector<int> a(n);
	vector<int> b(n);
	for(int i = 0; i < n; i++){
		cin >> a[i];
	}
	for(int i = 0; i < n; i++){
		cin >> b[i];
	}
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());
	int ans = 0;
	for(int i = 0; i < n; i++){
		if(b.back() > a.back()){
			b.pop_back();
			a.pop_back();
			ans++;
		}
		else{
			a.pop_back();
		}
	}
	cout << ans << endl;
}