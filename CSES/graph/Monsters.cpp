#include<bits/stdc++.h>
using namespace std;
#define MAX_INT 214748368
/*
TODO:
    出口座標
    回朔找路
*/
int v[4][2] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
char dir[4] = {'D', 'U', 'R', 'L'};
vector<char> ans;
int n, m;
bool dfs(int x, int y, vector<vector<int>> &monster_step, vector<vector<int>> &player_step, vector<vector<int>> &vis){
    //cout << x << ' ' << y << endl;
    vis[x][y] = 1;
    for(int i = 0; i < 4; i++){
        int next_x = x + v[i][0];
        int next_y = y + v[i][1];
        //cout << next_x << ' ' << next_y << endl;
        if(next_x >= 0 && next_x < n && next_y >= 0 && next_y < m && vis[next_x][next_y] == 0){
            if(player_step[next_x][next_y] == 0){
                ans.push_back(dir[i]);
                return true;
            }
            if(player_step[x][y] == player_step[next_x][next_y]+1 && monster_step[next_x][next_y] > player_step[next_x][next_y] && player_step[next_x][next_y] != MAX_INT){
                ans.push_back(dir[i]);
                if(dfs(next_x, next_y, monster_step, player_step, vis) == true){
                    return true;
                }
            }
            //ans.pop_back();
        }
    }
    return false;
}
int main(){
    ios_base::sync_with_stdio(0); cin.tie(0);
    cin >> n >> m;
    vector<string> G(n);
    for(int i = 0; i < n; i++){
        cin >> G[i];
    }
    vector<pair<int, int>> monsters_cord;
    pair<int, int> player_cord;
    vector<vector<int>> monster_step(n, vector<int>(m, MAX_INT));
    vector<vector<int>> player_step(n, vector<int>(m, MAX_INT));
    for(int i = 0; i < n; i++){
        for(int j =  0; j < m;  j++){
            if(G[i][j] == 'M'){
                monsters_cord.push_back({i, j});
                monster_step[i][j] = 0;
            }
            if(G[i][j] == 'A'){
                player_cord = {i, j};
                if(i == 0 || i == n-1 || j == 0 || j == n-1){
                    cout << "YES" << endl;
                    cout << "0" << endl;
                    return 0;
                }
                player_step[i][j] = 0;
            }
        }
    }
    queue<pair<int, int>> q;
    for(auto mon_cord : monsters_cord){
        q.push(mon_cord);
    }
    while(!q.empty()){
        int x = q.front().first;
        int y = q.front().second;
        // cout << "x: "<< x << " y: " << y << endl;
        for(int i = 0; i < 4; i++){
            int next_x = x + v[i][0];
            int next_y = y + v[i][1];
            // cout << "next_x: " << next_x << " next_y: " << next_y << endl;
            if(next_x < 0 || next_x >= n || next_y < 0 || next_y >= m){
                continue;
            }
            if(G[next_x][next_y] == '.' && monster_step[next_x][next_y] == MAX_INT){
                monster_step[next_x][next_y] = monster_step[x][y] + 1;
                q.push({next_x, next_y});
            }
        }
        q.pop();
    }
    q.push({player_cord});
    while(!q.empty()){
        int x = q.front().first;
        int y = q.front().second;
        for(int i = 0; i < 4; i++){
            int next_x = x + v[i][0];
            int next_y = y + v[i][1];
            if(next_x < 0 || next_x >= n || next_y < 0 || next_y >= m){
                continue;
            }
            if(G[next_x][next_y] == '.' && player_step[next_x][next_y] == MAX_INT){
                player_step[next_x][next_y] = player_step[x][y] + 1;
                q.push({next_x, next_y});
            }
        }
        q.pop();
    }
    vector<vector<int>> vis(n, vector<int>(m, 0));
    //cout << vis[4][7] << endl;
    //dfs(3, 7, monster_step, player_step, vis);
    //cout << player_step[3][7] << endl;
    // test
    /*
    cout << "Monster step: " << endl;
    for(auto i : monster_step){
        for(auto j : i){
            if(j == MAX_INT){
                cout << "." << ' ';
            }
            else{ 
                cout << j << ' ';
            }
        }
        cout << endl;
    }
    cout << "Player step: " << endl;
    for(auto i : player_step){
        for(auto j : i){
            if(j == MAX_INT){
                cout << "." << ' ';
            }
            else{ 
                cout << j << ' ';
            }
        }
        cout << endl;
    }
    */
    for(int i = 0; i < n; i++){
        if(player_step[i][0] != MAX_INT){
            if(dfs(i, 0, monster_step, player_step, vis) && player_step[i][0] < monster_step[i][0]){
                reverse(ans.begin(), ans.end());
                cout << "YES" << endl;
                cout << ans.size() << endl;
                for(auto j : ans){
                    cout << j;
                }
                cout << endl;
                return 0;
            }
            else{
                ans.clear();
            }
        }
        if(player_step[i][m-1] != MAX_INT){
            if(dfs(i, m-1, monster_step, player_step, vis) && player_step[i][m-1] < monster_step[i][m-1]){
                reverse(ans.begin(), ans.end());
                cout << "YES" << endl;
                cout << ans.size() << endl;
                for(auto j : ans){
                    cout << j;
                }
                cout << endl;
                return 0;
            }
            else{
                ans.clear();
            }
        }
    }
    for(int i = 0; i < m; i++){
        if(player_step[0][i] != MAX_INT && player_step[0][i] < monster_step[0][i]){
            if(dfs(0, i, monster_step, player_step, vis)){
                reverse(ans.begin(), ans.end());
                cout << "YES" << endl;
                cout << ans.size() << endl;
                for(auto j : ans){
                    cout << j;
                }
                cout << endl;
                return 0;
            }
            else{
                ans.clear();
            }
        }
        if(player_step[n-1][i] != MAX_INT){
            if(dfs(n-1, i, monster_step, player_step, vis) && player_step[n-1][i] < monster_step[n-1][i]){
                reverse(ans.begin(), ans.end());
                cout << "YES" << endl;
                cout << ans.size() << endl;
                for(auto j : ans){
                    cout << j;
                }
                cout << endl;
                return 0;
            }
            else{
                ans.clear();
            }
        }
    }
    cout << "NO" << endl;
    

}
