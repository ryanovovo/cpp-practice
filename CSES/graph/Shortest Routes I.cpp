#include<bits/stdc++.h>
using namespace std;

#define ll long long int

int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	class node{
		public:
		long long int u;
		long long int v;
		long long int weight;
	};
	int n, m;
	const long long int MAX_INT = 1844674407370955161;
	cin >> n >> m;
	vector<node> edges;
	vector<vector<int>> G(n+1);
	for(int i = 0; i < m; i++){
		long long int u, v, w;
		cin >> u >> v >> w;
		edges.push_back({u, v, w});
		G[u].push_back(i);
	}
	vector<long long int> shortest_path(n+1, MAX_INT);
	shortest_path[1] = 0;
	priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, greater<pair<ll, ll>>> pq;
	pq.push({0, 1});
	// long long int mn = MAX_INT;
	while(!pq.empty()){
		long long int root = pq.top().second;
		ll dist = pq.top().first;
		pq.pop();
		if(dist > shortest_path[root]){
			continue;
		}
		for(int i : G[root]){
			long long int u = edges[i].u;
			long long int v = edges[i].v;
			long long int w = edges[i].weight;
			if(shortest_path[v] > shortest_path[u] + w){
				shortest_path[v] = shortest_path[u] + w;
				if(G[v].empty()){
					continue;
				}
				pq.push({shortest_path[v], v});
				// mn = min(shortest_path[v], mn);
			}
		}
	}
	for(int i = 1; i <= n; i++){
		cout << shortest_path[i] << ' ';
	}
	cout << '\n';
}