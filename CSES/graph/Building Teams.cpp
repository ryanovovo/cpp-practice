#include<bits/stdc++.h>
using namespace std;
bool dfs(int root, vector<vector<int>> &G, vector<int> &vis){
	for(int ch : G[root]){
		// cout << "root: " << root << ' ' << "ch: " << ch << endl;
		if(vis[ch] == vis[root]){
			return false;
		}
		else if(vis[ch] == -1){
			vis[ch] = (vis[root]^1);
			if(dfs(ch, G, vis) == false){
				return false;
			}
		}
	}
	return true;
}
int main(){
	int n, m;
	cin >> n >> m;
	vector<vector<int>> G(n+1);
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		G[t].push_back(s);
	}
	vector<int> vis(n+1, -1);
	for(int i = 1; i <= n; i++){
		if(vis[i] == -1){
			vis[i] = 0;
			if(dfs(i, G, vis) == false){
				cout << "IMPOSSIBLE" << endl;
				return 0;
			}
		}
	}
	cout << endl;
	for(auto i : vis){
		if(i == 0){
			cout << "1 ";
		}
		else if(i == 1){
			cout << "2 ";
		}
	}
}