#include<bits/stdc++.h>
using namespace std;
void dfs(int root, vector<int> &vis, vector<vector<int>> &G, const int &rep){
	vis[root] = rep;
	//cout << root << endl;
	for(int ch : G[root]){
		if(vis[ch] == 0){
			// vis[ch] = rep;
			dfs(ch, vis, G, rep);
		}
	}
	return;
}
int main(){
	int n, m;
	cin >> n >> m;
	vector<int> vis(n+1, 0);
	vector<vector<int>> G(n+1);
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		G[t].push_back(s); 
	}
	for(int i = 1; i <= n; i++){
		if(vis[i] == 0){
			dfs(i, vis, G, i);
		}
	}

	cout << endl;
	set<int> s;
	for(int i = 1; i <= n; i++){
		s.insert(vis[i]);
	}
	vector<int> ans;
	for(auto i : s){
		ans.push_back(i);
	}
	cout << ans.size() - 1 << endl;
	for(int i = 1; i < ans.size(); i++){
		cout << ans[0] << ' ' << ans[i] << endl;
	}

}