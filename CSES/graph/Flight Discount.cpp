#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define INF 1844674407370955161
int main(){
	int n, m;
	cin >> n >> m;
	vector<vector<pair<int, int>>> G(2*n+1); //dest, cost
	for(int i = 0; i < m; i++){
		int s, t, c;
		cin >> s >> t >> c;
		G[s].push_back({t, c});
		G[s+n].push_back({t+n, c});
		G[s].push_back({t+n, c/2});
	}
	vector<ll> shortest_path(2*n+1, INF); // node shortest_path, node idx
	shortest_path[1] = 0;
	priority_queue<pair<ll, ll>, vector<pair<ll, ll>>, greater<pair<ll, ll>>> pq;
	pq.push({0, 1});
	while(shortest_path[2*n] > pq.top().first){
		// ll sp = pq.top().first;
		ll s = pq.top().second;
		// cout << s << endl;
		pq.pop();
		for(auto ch : G[s]){
			int t = ch.first;
			int c = ch.second;
			if(shortest_path[s] + c < shortest_path[t]){
				shortest_path[t] = shortest_path[s] + c;
				pq.push({shortest_path[t], t});
			}
		}
	}
	/*
	for(auto i : shortest_path){
		cout << i << ' ';
	}
	cout << endl;
	*/
	cout << shortest_path[2*n] << endl;
}
