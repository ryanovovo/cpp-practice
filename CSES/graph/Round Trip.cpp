#include<bits/stdc++.h>
using namespace std;
vector<int> ans;
bool dfs(int root, vector<vector<int>> &G, vector<int> &vis){
	for(int ch : G[root]){
		if(vis[root] == ch){
			continue;
		}
		else if(vis[ch] != 0){
			ans.push_back(ch);
			return true;
		}
		else{
			ans.push_back(ch);
			vis[ch] = root;
			if(dfs(ch, G, vis)){
				return true;
			}
		}
	}
	ans.pop_back();
	// vis[root] = 0;
	return false;
}
int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	int n, m;
	cin >> n >> m;
	vector<vector<int>> G(n+1);
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		G[t].push_back(s);
	}
	vector<int> vis(n+1, 0);
	for(int i = 1; i <= n; i++){
		if(vis[i] != 0){
			continue;
		}
		ans.clear();
		// vis = vector<int>(n+1, 0);
		ans.push_back(i);
		dfs(i, G, vis);
		if(!ans.empty()){
			break;
		}
	}
	vis = vector<int>(n+1, -1);
	int st = 0;
	int end = 0;
	for(int i = 0; i < (int)ans.size(); i++){
		if(vis[ans[i]] != -1){
			st = vis[ans[i]];
			end = i;
			break;
		}
		else{
			vis[ans[i]] = i;
		}
	}
	/*
	cout << "ans: ";
	for(int i : ans){
		cout << i << ' ';
	}
	cout << endl;
	cout << "res: ";
	for(int i : vis){
		cout << i << ' ';
	}
	cout << endl;
	cout << "st: " << st << endl;
	cout << "end: " << end << endl;
	*/
	if(st == 0 && end == 0){
		cout << "IMPOSSIBLE" << endl;
		return 0;
	}
	cout << end - st + 1 << endl;
	for(int i = st; i <= end; i++){
		cout << ans[i] << ' ';
	}
	cout << endl;
}