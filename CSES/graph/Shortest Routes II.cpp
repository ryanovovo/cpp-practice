#include<bits/stdc++.h>
using namespace std;
#define ll long long int
int main(){
	const ll MAX_INT = 1844674407370955161;
	ll n, m, q;
	cin >> n >> m >> q;
	vector<vector<ll>> d(n+1, vector<ll>(n+1, MAX_INT));
	for(ll i = 0; i < m; i++){
		ll s, t, c;
		cin >> s >> t >> c;
		d[s][t] = min(c, d[s][t]);
		d[t][s] = min(c, d[t][s]);
	}
	for(int i = 1; i <=  n; i++){
		d[i][i] = 0;
	}
	for(ll k = 1; k <= n; k++){
		for(ll i = 1; i <= n; i++){
			for(ll j = 1; j <= n; j++){
				d[i][j] = min(d[i][j], d[i][k]+d[k][j]);
				d[j][i] = d[i][j];
			}
		}
	}
	while(q--){
		ll s, t;
		cin >> s >> t;
		if(d[s][t] == MAX_INT){
			cout << "-1" << endl;
			// cout << d[s][t] << endl;
		}
		else{
			cout << d[s][t] << endl;
		}
	}
}