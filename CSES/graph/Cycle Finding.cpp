#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define INF 92233720368547758
class edge{
public:
	int s, t, c;
};
int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(0);
	int n, m;
	cin >> n >> m;
	vector<edge> E;
	for(int i = 0; i < m; i++){
		int s, t, c;
		cin >> s >> t >> c;
		E.push_back({s, t, c});
	}
	for(int i = 1; i <= n; i++){
		E.push_back({0, i, 0});
	}
	vector<ll> sp(n+1, 0);
	sp[0] = 0;
	vector<int> relaxant(n+1, -1);
	int last_relaxant = 0;
	for(int i = 0; i < n; i++){
		for(auto e : E){
			int s = e.s;
			int t = e.t;
			int c = e.c;
			if(sp[t] > sp[s] + c){
				sp[t] = sp[s] + c;
				relaxant[t] = s;
				last_relaxant = t;
			}
		}
	}
	vector<int> ans;
	ans.push_back(last_relaxant);
	int root = last_relaxant;
	for(int i = 0; i < n; i++){
		if(relaxant[root] == -1){
			cout << "NO" << endl;
			return 0;
		}
		if(relaxant[root] == ans[0]){
			ans.push_back(relaxant[root]);
			break;
		} 
		else{
			ans.push_back(relaxant[root]);
			root = relaxant[root];
		}
	}
	reverse(ans.begin(), ans.end());
	if(ans.size() == 1){
		cout << "NO" << endl;
		return 0;
	}
	vector<int> vis(n+1, -1);
	int st;
	int end;
	for(unsigned int i = 0; i < ans.size(); i++){
		if(vis[ans[i]] == -1){
			vis[ans[i]] = i;
		}
		else{
			st = vis[ans[i]];
			end = i;
			break;
		}
	}
	cout << "YES" << endl;
	for(int i = st; i <= end; i++){
		cout << ans[i] << ' ';
	}
	cout << endl;
}