#include<bits/stdc++.h>
using namespace std;

int main(){
	const int INF = 0x3f3f3f3f;
	int n, m;
	cin >> n >> m;
	vector<vector<int>> G(n+1);
	vector<int> indegree(n+1, 0);
	vector<pair<int, int>> dp(n+1, {-INF, 0});
	dp[1] = {1, 0};
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		indegree[t]++;
	}
	queue<int> q;
	vector<int> ans;
	for(int i = 1; i <= n; i++){
		if(indegree[i] == 0){
			q.push(i);
		}
	}
	while(!q.empty()){
		int f = q.front();
		ans.push_back(f);
		for(auto ch : G[f]){
			indegree[ch]--;
			if(dp[f].first + 1 > dp[ch].first){
				dp[ch] = {dp[f].first+1, f};
			}
			if(indegree[ch] == 0){
				q.push(ch);
			}
		}
		q.pop();
	}
	if(dp[n].first < 0){
		cout << "IMPOSSIBLE" << endl;
		return 0;
	}
	vector<int> res;
	int root = n;
	for(int i = 0; i < n; i++){
		res.push_back(root);
		root = dp[root].second;
		if(root == 1){
			break;
		}
	}
	res.push_back(1);
	reverse(res.begin(), res.end());
	cout << res.size() << endl;
	for(auto i : res){
		cout << i <<' ';
	}
	cout << endl;
}
