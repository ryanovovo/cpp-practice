#include<bits/stdc++.h>
using namespace std;
vector<int> find_tl_order(vector<vector<int>> &G){
	int n = G.size();
	vector<int> indegree(n, 0);
	for(auto i : G){
		for(auto ch : i){
			indegree[ch]++;
		}
	}
	indegree[1] = 0;
	vector<int> res;
	res.push_back(1);
	queue<int> q;
	q.push(1);
	for(int i = 2; i < n; i++){
		if(indegree[i] == 0){
			q.push(i);
		}
	}
	while(!q.empty()){
		int root = q.front();
		// cout << root << endl;
		q.pop();
		for(auto ch : G[root]){
			// cout << ch << endl;
			indegree[ch]--;
			if(indegree[ch] == 0){
				q.push(ch);
				res.push_back(ch);
			}
			//cout << indegree[ch] << endl;
		}
	}
	return res;
}
int main(){
	int n, m;
	cin >> n >> m;
	vector<vector<int>> G(n+1);
	vector<vector<int>> rev_G(n+1);
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		rev_G[t].push_back(s);
	}
	vector<int> dp(n+1, 0);
	dp[1] = 1;
	vector<int> tl = find_tl_order(G);
	for(auto i : tl){
		for(auto anc : rev_G[i]){
			dp[i] += (dp[anc] % 1000000007);
			dp[i] %= 1000000007;
		}
	}
	cout << dp[n] << endl;
}