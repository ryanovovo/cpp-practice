#include<bits/stdc++.h>
using namespace std;

int main(){
	const int INF = 0x3f3f3f3f;
	int n, m;
	cin >> n >> m;
	vector<string> v(n);
	vector<vector<int>> step(n, vector<int>(m, INF));
	for(int i = 0; i < n; i++){
		cin >> v[i];
	}
	vector<int> cord_A(2, 0);
	vector<int>	cord_B(2, 0);
	queue<pair<int, int>> q;
	int s = 0;
	int dir[4][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
	for(int i = 0; i < n; i++){
		for(int j = 0;  j < m; j++){
			if(v[i][j] == 'A'){
				cord_A[0] = i;
				cord_A[1] = j;
			}
			if(v[i][j] == 'B'){
				step[i][j] = s;
				cord_B[0] = i;
				cord_B[1] = j;
			}
		}
	}
	for(int i = 0; i < n; i++){
		for(int j = 0;  j < m; j++){
			if(v[i][j] == 'A'){
				cord_A[0] = i;
				cord_A[1] = j;
				q.push({i, j});
				step[i][j] = 0;
				while(!q.empty()){
					int x = q.front().first;
					int y = q.front().second;
					//step[x][y] = s;
					//v[x][y] = '#';
					for(int k = 0; k < 4; k++){
						int new_x = x + dir[k][0];
						int new_y = y + dir[k][1];
						if(new_x >= 0 && new_x < n && new_y >= 0 && new_y < m && (v[new_x][new_y] == '.' || v[new_x][new_y] == 'B')){
							step[new_x][new_y] = step[x][y] + 1;
							q.push({new_x, new_y});
							v[new_x][new_y] = '#';
						}
					}
					s++;
					q.pop();
				}
			}
		}
	}
	string ans;
	while (!q.empty()) q.pop();
	if(step[cord_B[0]][cord_B[1]] == 0){
		cout << "NO" << endl;
		return 0;
	}
	else{
		cout << "YES" << endl;
		cout << step[cord_B[0]][cord_B[1]] << endl;
		q.push({cord_B[0], cord_B[1]});
		while(!q.empty()){
			int x = q.front().first;
			int y = q.front().second;
			for(int k = 0; k < 4; k++){
				int new_x = x + dir[k][0];
				int new_y = y + dir[k][1];
				if(new_x >= 0 && new_x < n && new_y >= 0 && new_y < m && step[new_x][new_y] == step[x][y] - 1){
					if(k == 0){
						ans.push_back('U');
					}
					else if(k == 1){
						ans.push_back('D');
					}
					else if(k == 2){
						ans.push_back('L');
					}
					else if(k == 3){
						ans.push_back('R');
					}
					q.push({new_x, new_y});
					break;
				}				
			}
			q.pop();
		}
		reverse(ans.begin(), ans.end());
		cout << ans << endl;
	}
}