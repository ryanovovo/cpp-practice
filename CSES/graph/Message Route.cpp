#include<bits/stdc++.h>
using namespace std;
int main(){
	int n, m;
	cin >> n >> m;
	vector<vector<int>> G(n+1);
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		G[t].push_back(s);
	}
	vector<int> vis(n+1, 0);
	queue<int> q;
	q.push(1);
	vis[1] = 0;
	while(!q.empty()){
		int root = q.front();
		for(auto ch : G[root]){
			if(vis[ch] == 0){
				q.push(ch);
				vis[ch] = vis[root] + 1;
			}
		}
		q.pop();
	}
	while(!q.empty()){
		q.pop();
	}
	q.push(n);
	vector<int> ans;
	if(vis[n] == 0){
		cout << "IMPOSSIBLE" << endl;
	}
	else{
		while(!q.empty()){
			int root = q.front();
			ans.push_back(root);
			for(auto ch : G[root]){
				if(vis[ch] == vis[root]-1){
					q.push(ch);
					break;
				}
			}
			q.pop();
		}
		ans.push_back(1);
		reverse(ans.begin(), ans.end());
		cout << ans.size() << endl;
		for(auto i : ans){
			cout << i << ' ';
		}
		cout << endl;
	}
}