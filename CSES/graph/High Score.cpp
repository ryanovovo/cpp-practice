#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define MAX_INT 1844674407370955161
class edge{
public:
	int s, t, c;
};
int main(){
	int n, m;
	cin >> n >> m;
	vector<edge> E;
	for(int i = 0; i < m; i++){
		int s, t, c;
		cin >> s >> t >> c;
		E.push_back({s, t, -c});
	}
	vector<ll> sp(n+1, MAX_INT);
	sp[1] = 0;
	vector<int> relaxant(n+1, -1);
	for(int i = 0; i < n; i++){
		for(auto e : E){
			int s = e.s;
			int t = e.t;
			int c = e.c;
			if(sp[t] > sp[s] + c){
				sp[t] = sp[s] + c;
				relaxant[t] = s;
			}
		}
	}
	ll ans = sp[n];
	int root = n;
	vector<int> vis(n+1, 0);
	for(int i = 0; i <= n; i++){
		if(relaxant[root] == -1){
			break;
		}
		if(vis[root] == 1){
			cout << "-1" << endl;
			return 0;
		}
		else{
			vis[root] = 1;
			root = relaxant[root];
		}
	}
	cout << -ans << endl;
}