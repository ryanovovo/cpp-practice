#include<bits/stdc++.h>
using namespace std;
int main(){
	int n, m;
	cin >> n >> m;
	vector<vector<int>> G(n+1);
	vector<int> indegree(n+1, 0);
	for(int i = 0; i < m; i++){
		int s, t;
		cin >> s >> t;
		G[s].push_back(t);
		indegree[t]++;
	}
	queue<int> q;
	vector<int> ans;
	for(int i = 1; i <= n; i++){
		if(indegree[i] == 0){
			q.push(i);
		}
	}
	while(!q.empty()){
		int f = q.front();
		ans.push_back(f);
		for(auto ch : G[f]){
			indegree[ch]--;
			if(indegree[ch] == 0){
				q.push(ch);
			}
		}
		q.pop();
	}
	if(ans.size() == n){
		for(auto i : ans){
			cout << i << ' ';
		}
		cout << endl;
	}
	else{
		cout << "IMPOSSIBLE" << endl;
	}
}