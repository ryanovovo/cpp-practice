#include<bits/stdc++.h>
using namespace std;
int main(){
	int n, m;
	cin >> n >> m;
	vector<string> v(n);
	for(int i = 0; i < n; i++){
		cin >> v[i];
	}
	queue<pair<int, int>> q;
	int cnt = 0;
	int dir[4][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(v[i][j] == '.'){
				q.push({i, j});
				while(!q.empty()){
					int x = q.front().first;
					int y = q.front().second;
					v[x][y] = '#';
					for(int k = 0; k < 4; k++){
						int new_x = x + dir[k][0];
						int new_y = y + dir[k][1];
						if(new_x >= 0 && new_x < n && new_y >= 0 && new_y < m && v[new_x][new_y] == '.'){
							q.push({new_x, new_y});
							v[new_x][new_y] = '#';
						}
					}
					q.pop();
				}
				cnt++;
			}
		}
	}
	cout << cnt << endl;
}