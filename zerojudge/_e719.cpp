#include <bits/stdc++.h>
using namespace std;
set<pair<int, int>> v;
inline int query(int lx, int ux, int ly, int uy){
	int midx = (lx + ux)/2;
	int midy = (ly + uy)/2;
	if(lx == ux && ly == uy){
		if(v.find({lx, ly}) == v.end()){
			return 1;
		}
		else{
			return 0;
		}
	}
	int tl = query(lx, midx, midy+1, uy);
	int tr = query(midx+1, ux, midy+1, uy);
	int bl = query(lx, midx, ly, midy);
	int br = query(midx+1, ux, ly, midy);
	return tl+tr+bl+br;
}
int main(){
	int radius, n;
	cin >> radius >> n;
	long long int ans = 0;
	for(int i = 0; i < n; i++){
		pair<int, int> vertices;
		cin >> vertices.first >> vertices.second;
		v.insert(vertices);
	}
	for(auto vertices : v){
		int lbx = max(vertices.first-radius, -2147483647);
		int ubx = min(vertices.first+radius, 2147483647);
		int lby = max(vertices.second-radius, -2147483647);
		int uby = min(vertices.second+radius, 2147483647);
		ans += query(lbx, ubx, lby, uby)-1;
	}
	cout  << ans/2 << endl;
}