#include <bits/stdc++.h>
using namespace std;
int main(){
	int n, m, l;
	cin >> m >> n >> l;
	int numx, numy, x, y, completed;
	numx = m/l;
	numy = n/l;
	x = n%l;
	y = m%l;
	completed = numx*numy;
	if(x > y){
		if(y != 0){
			numy++;
		}
	}
	else{
		if(x != 0){
			numx++;
		}
	}
	//cout << x << " " << y << " " << numx << " " << numy <<  " " << completed << endl;
	vector<vector<pair<int, priority_queue<int>>>> dp(numx+1, vector<pair<int, priority_queue<int>>>(numy+1));
	dp[0][0].first = 0;
	dp[0][0].second.push(0);
	for(int i = 1; i <= numx; i++){
		pair<int, priority_queue<int>> res;
		res.second = dp[i-1][0].second;
		if(dp[i-1][0].second.top() < x){
			res.first = dp[i-1][0].first+1;
			res.second.push(l - x);
		}
		else{
			res.first = dp[i-1][0].first;
			res.second.push(dp[i-1][0].second.top() - x);
			res.second.pop();
		}
		dp[i][0] = res;
	}
	for(int j = 1; j <= numy; j++){
		pair<int, priority_queue<int>> res;
		res.second = dp[0][j-1].second;
		if(dp[0][j-1].second.top() < y){
			res.first = dp[0][j-1].first+1;
			res.second.push(l - y);
		}
		else{
			res.first = dp[0][j-1].first;
			res.second.push(dp[0][j-1].second.top() - y);
			res.second.pop();
		}
		dp[0][j] = res;
	}
	for(int i = 1; i <= numx; i++){
		for(int j = 1; j <= numy; j++){
			pair<int, priority_queue<int>> choose_x;
			pair<int, priority_queue<int>> choose_y;
			choose_x.second = dp[i-1][j].second;
			choose_y.second = dp[i][j-1].second;
			if(dp[i-1][j].second.top() < x){
				choose_x.first = dp[i-1][j].first+1;
				choose_x.second.push(l - x);
			}
			else{
				choose_x.first = dp[i-1][j].first;
				choose_x.second.push(dp[i-1][j].second.top() - x);
				choose_x.second.pop();
			}
			if(dp[i][j-1].second.top() < y){
				choose_y.first = dp[i][j-1].first+1;
				choose_y.second.push(l - y);
			}
			else{
				choose_y.first = dp[i][j-1].first;
				choose_y.second.push(dp[i][j-1].second.top() - y);
				choose_y.second.pop(); 
			}
			if(choose_x.first > choose_y.first){
				dp[i][j] = choose_y;
			}
			else if(choose_x.first < choose_y.first){
				dp[i][j] = choose_x;
			}
			else if(choose_x.second.top() > choose_y.second.top()){
				dp[i][j] = choose_x;
			}
			else{
				dp[i][j] = choose_y;
			}
			//cout << i << " " << j << " " << dp[i][j].first << " " << dp[i][j].second.top() << endl;
		}
	}
	cout << dp[numx][numy].first + completed << endl;
}