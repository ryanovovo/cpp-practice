#include <bits/stdc++.h>
using namespace std;
int ans = 0;
int n, m;
pair<int, int> move(const int & dir, const int &x, const int &y, vector<vector<int>> &board){
	if(dir == 0){
		int cnt = 0;
		pair<int, int>res = {-1, -1};
		for(int i = y; i >= 0; i--){
			if(board[x][i] == 1){
				cnt++;
			}
			if(cnt == 2){
				res.first = x;
				res.second = i;
				return res;
			}
		}
		return res;
	}
	else if(dir == 1){
		int cnt = 0;
		pair<int, int>res = {-1, -1};
		for(int i = x; i < m; i++){
			if(board[i][y] == 1){
				cnt++;
			}
			if(cnt == 2){
				res.first = i;
				res.second = y;
				return res;
			}
		}
		return res;
	}
	else if(dir == 2){
		int cnt = 0;
		pair<int, int>res = {-1, -1};
		for(int i = y; i < n; i++){
			if(board[x][i] == 1){
				cnt++;
			}
			if(cnt == 2){
				res.first = x;
				res.second = i;
				return res;
			}
		}
		return res;
	}
	else if(dir == 3){
		int cnt = 0;
		pair<int, int>res = {-1, -1};
		for(int i = x; i >= 0; i--){
			if(board[i][y] == 1){
				cnt++;
			}
			if(cnt == 2){
				res.first = i;
				res.second = y;
				return res;
			}
		}
		return res;
	}
}
void dfs(const int &x, const int &y, vector<vector<int>> board, int score){
	for(int dir = 0; dir < 4; dir++){
		pair<int, int> mv = move(dir, x, y, board);
		if(mv.first != -1){
			board[mv.first][mv.second] = 0;
			ans = max(score+1, ans);
			dfs(mv.first, mv.second, board, score+1);
			board[mv.first][mv.second] = 1;
		}
	}
	return;
}
int main(){
	cin >> n >> m;
	vector<vector<int>> board(m, vector<int>(n, 1));
	board[0][0] = 0;
	dfs(0, 0, board, 0);
	cout << ans << endl;
}