#include<bits/stdc++.h>
using namespace std;
class segtree_node{
public:
	segtree_node *lhs;
	segtree_node *rhs;
	int sum;
	int l, r;
	bool flag;
	segtree_node(){
		sum = 0;
		lhs = nullptr;
		rhs = nullptr;
		flag = false;
	}
	segtree_node(int x){
		sum = x;
		lhs = nullptr;
		rhs = nullptr;
		flag = false;
	}
};
inline segtree_node* merge(segtree_node *a, segtree_node *b){
	if(a == nullptr){
		return b;
	}
	else if(b == nullptr){
		return a;
	}
	segtree_node *ret = new segtree_node;
	ret->lhs = a;
	ret->rhs = b;
	ret->sum = a->sum+b->sum;
	ret->l = min(a->l, b->l);
	ret->r = max(a->r, b->r);
	return ret;
}
inline segtree_node* build_tree(const vector<int> &v, int l, int r){
	int mid = (l+r)/2;
	if(l == r){
		segtree_node *root = new segtree_node;
		root->rhs = nullptr;
		root->lhs = nullptr;
		root->sum = v[l];
		root->l = l;
		root->r = r;
		return root;
	}
	else{
		return merge(build_tree(v, l, mid), build_tree(v, mid+1, r));
	}
}
inline int query_sum(segtree_node *root, int l, int r){
	if(root == nullptr){
		return 0;
	}
	int mid = (root->l + root->r)/2;
	if(root->l == l && root->r == r){
		return root->sum;
	}
	else{
		if(mid < l){
			return query_sum(root->rhs, l, r);
		}
		else if(mid >= r){
			return query_sum(root->lhs, l, r);
		}
		else{
			return query_sum(root->lhs, l, mid) + query_sum(root->rhs, mid+1, r);
		}
	}
}
inline void pull(segtree_node *root){
	if(root == nullptr || root->flag == false){
		return;
	}
	pull(root->lhs);
	pull(root->rhs);
	if(root->lhs == nullptr && root->rhs == nullptr){
		return;
	}
	else if(root->lhs == nullptr && root->rhs != nullptr){
		root->sum = root->rhs->sum;
	}
	else if(root->lhs != nullptr && root->rhs == nullptr){
		root->sum = root->lhs->sum;
	}
	else{
		root->sum = root->lhs->sum + root->rhs->sum;
	}
	root->flag = false;
	return;
}
inline void update_tree(segtree_node *root, int idx, int val){
	if(root == nullptr){
		return;
	}
	int l = root->l;
	int r = root->r;
	int mid = (l + r) / 2;
	if(l == r && r == idx){
		root -> sum = val;
	}
	else{
		if(mid < idx){
			update_tree(root->rhs, idx, val);
		}
		else{
			update_tree(root->lhs, idx, val);
		}
	}
	root->flag = true;
	pull(root);
	return;
}
void pt(segtree_node *root){
	if(root == nullptr){
		return;
	}
	pt(root->lhs);
	cout << " l: "<<root->l << " r: " << root->r << " sum: " << root->sum << endl;
	pt(root->rhs);
	return;
}
int main(){
	int n;
	cin >> n;
	vector<vector<int>> arr(n+1);
	for(int i = 0; i < 2*n; i++){
		int tmp;
		cin >> tmp;
		arr[tmp].push_back(i);
	}
	vector<int> v(2*n, 0);
	long long int ans = 0;
	segtree_node *root = build_tree(v, 0, 2*n-1);
	for(int i = 1; i <= n; i++){
		//pt(root);
		int l = arr[i][0];
		int r = arr[i][1];
		ans += query_sum(root, l, r);
		update_tree(root, l, 1);
		update_tree(root, r, 1);
	}
	cout << ans << endl;
}