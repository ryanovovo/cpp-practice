#include<bits/stdc++.h>
using namespace std;
int r, c, k, m;
void add_arr(vector<vector<int>> &v, vector<vector<int>> &add){
	for(int i = 1; i <= r; i++){
		for(int j = 1; j <= c; j++){
			v[i][j] += add[i][j];
			//cout << v[i][j] << ' ';
			add[i][j] = 0;
		}
		//cout << endl;
	}
	return;
}
void pt(vector<vector<int>> &v){
	for(int j = 0; j <= c+1; j++){
		for(int i = 0; i <= r+1; i++){
			cout << v[i][j] << ' ';
		}
		cout << endl;
	}
	return;
}
int main(){
	cin >> c >> r >> k >> m;
	vector<vector<int>> v(r+2, vector<int>(c+2, -1));
	vector<vector<int>> add(r+2, vector<int>(c+2, 0));
	for(int j = 1; j <= c; j++){
		for(int i = 1; i <= r; i++){
			cin >> v[i][j];
		}
	}
	while(m--){
		for(int i = 1; i <= r; i++){
			for(int j = 1; j <= c; j++){
				if(v[i-1][j] >= 0){
					add[i][j] -= (v[i][j]/k);
					add[i-1][j] += (v[i][j]/k);
				}
				if(v[i+1][j] >= 0){
					add[i][j] -= (v[i][j]/k);
					add[i+1][j] += (v[i][j]/k);
				}
				if(v[i][j-1] >= 0){
					add[i][j] -= (v[i][j]/k);
					add[i][j-1] += (v[i][j]/k);
				}
				if(v[i][j+1] >= 0){
					add[i][j] -= (v[i][j]/k);
					add[i][j+1] += (v[i][j]/k);
				}
			}
		}
		//pt(add);
		add_arr(v, add);
		//pt(v);
		//pt(add);
	}
	int mn = 999999999;
	int mx = -999999999;
	for(auto i : v){
		for(auto j : i){
			if(j != -1){
				mn = min(mn, j);
			}
			mx = max(mx, j);
		}
	}
	cout << mn << endl;
	cout << mx << endl;

}
