#include <bits/stdc++.h>
using namespace std;
long long int n, height, mx;
long long int ans = 0;
void solve(vector<long long int> &bottles){
	for(long long int i = 1; i < n; i++){
		long long int diff = bottles[i] - bottles[i-1] - mx;
		if(diff >= 0){
			ans += (diff/height + 1);
			bottles[i] -= (diff/height + 1)*height;
		}
	}
}
int main(){
	cin >> n >> height >> mx;
	vector<long long int> bottles(n, 0);
	for(long long int i = 0; i < n; i++){
		cin >> bottles[i];
	}
	solve(bottles);
	reverse(bottles.begin(), bottles.end());
	solve(bottles);
	cout << ans << endl;
}