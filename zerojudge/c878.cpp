#include <bits/stdc++.h>
using namespace std;
int main(){
	int query;
	long long int ans = 0;
	cin >> query;
	vector<vector<long long int>> dp(query+1, vector<long long int>(3, 0));
	vector<long long int> v(query+1, 0);
	dp[1][0] = 1;
	dp[1][1] = 1;
	v[1] = 2;
	v[0] = 1;
	for(int i = 2; i <= query; i++){
		dp[i][0] = (dp[i-1][2] + dp[i-1][1] + dp[i-1][0])%100000007;
		dp[i][1] = dp[i-1][0];
		dp[i][2] = dp[i-1][1];
		v[i] = (dp[i][0] + dp[i][1] + dp[i][2])%100000007;
	}
	for(int i = 1; i <= query; i++){
		//cout << v[i] << endl;
		ans += ((v[i-1]%100000007)*(v[query-i]%100000007))%100000007;
	}
	ans = (ans + v[query])%100000007;
	cout << ans << endl;
}