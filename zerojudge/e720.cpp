#include <bits/stdc++.h>
using namespace std;
vector<vector<int>> edges(2000);
vector<vector<int>> graph(2000, vector<int> (2000, 0));
inline void dfs(int start, vector<int> &visited, int &nodes){
	for(int v: edges[start]){
		if(graph[start][v] == 1 && visited[v] == 0){
			nodes++;
			visited[v] = 1;
			dfs(v, visited, nodes);
		}
	}
	return;
}
int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	int num_edges = 0;
	int ans = 0;
	int num_nodes = 0;
	cin >> num_nodes >> num_edges;
	for(int i = 0; i < num_edges; i++) {
		int start, dest;
		cin >> start >> dest;
		graph[start][dest] = 1;
		graph[dest][start] = 1;
		edges[start].push_back(dest);
		edges[dest].push_back(start);
	}
	//cout << "number of nodes " << num_nodes << endl;
	for(int i = 0; i < num_nodes; i++){
		for(int v: edges[i]){
			int nodes = 1;
			vector<int> visited(2000, 0);
			visited[i]  = 1;
			graph[i][v] = 0;
			graph[v][i] = 0;
			dfs(i, visited, nodes);
			graph[i][v] = 1;
			graph[v][i] = 1;
			//cout << nodes << endl;
			ans = max(ans, (num_nodes-nodes)*nodes);
		}
	}
	cout << ans << endl;
}