#include <bits/stdc++.h>
using namespace std;
struct vertices{
	int x;
	int y;
	int idx;
};
inline bool cmpx(const vertices &lhs, const vertices &rhs){
	return lhs.x < rhs.x;
}
inline bool cmpy(const vertices &lhs, const vertices &rhs){
	return lhs.y < rhs.y;
}
inline pair<int ,int> queryx(int lb, int ub,int ll, int rl, int lu, int ru, vector<vertices> &vx, bool flagl, bool flagu){
	//cout << ll<< ' ' << rl << ' ' << lu << ' ' << ru << endl;
	if(rl-ll <= 1 && ru-lu <= 1){
		return {ll, ru};
	}
	int midl = (ll+rl)/2;
	int midu = (lu+ru)/2;
	if(vx[ll].x >= lb && flagl == false){
		ll = ll - 1;
		rl = ll;
		flagl = true;
	}
	else if(vx[midl].x <= lb){
		ll = midl;
	}
	else if(vx[midl].x >= lb){
		rl = midl;
	}
	if(vx[ru].x <= ub && flagu == false){
		ru = ru + 1;
		lu = ru;
		flagu = true;
	}
	else if(vx[midu].x <= ub){
		lu = midu;
	}
	else if(vx[midu].x >= ub){
		ru = midu;
	}
	queryx(lb, ub, ll, rl, lu, ru, vx, flagl, flagu);
}
pair<int ,int> queryy(int lb, int ub,int ll, int rl, int lu, int ru, vector<vertices> &vy, bool flagl, bool flagu){
	//cout << ll<< ' ' << rl << ' ' << lu << ' ' << ru << endl;
	if(rl-ll <= 1 && ru-lu <= 1){
		return {ll, ru};
	}
	int midl = (ll+rl)/2;
	int midu = (lu+ru)/2;
	if(vy[ll].y >= lb && flagl == false){
		ll = ll - 1;
		rl = ll;
		flagl = true;
	}
	else if(vy[midl].y <= lb){
		ll = midl;
	}
	else if(vy[midl].y >= lb){
		rl = midl;
	}
	if(vy[ru].y <= ub && flagu == false){
		ru = ru + 1;
		lu = ru;
		flagu = true;
	}
	else if(vy[midu].y <= ub){
		lu = midu;
	}
	else if(vy[midu].y >= ub){
		ru = midu;
	}
	queryy(lb, ub, ll, rl, lu, ru, vy, flagl, flagu);
}
int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	int n, radius;
	long long int ans = 0;
	cin >> radius >> n;
	vector<vertices> v(n);
	for(int i = 0; i < n; i++){
		int x, y;
		cin >> x >> y;
		v[i].x = x;
		v[i].y = y;
		v[i].idx = i;
	}
	vector<vertices> vx = v;
	vector<vertices> vy = v;
	sort(vx.begin(), vx.end(), cmpx);
	sort(vy.begin(), vy.end(), cmpy);
	/*for(auto itx: vx){
		cout << itx.x << ' ';
	}
	cout << endl;
	for(auto ity: vy){
		cout << ity.y << ' ';
	}
	cout << endl;*/
	vector<set<int>> visited(n);
	for(auto it : v){
		set<int> _x;
		set<int> _y;
		set<int> intersection;
		pair<int, int> rangex;
		pair<int, int> rangey;
		int x = it.x;
		int y = it.y;
		int lbx = max(x-radius, -2147483647);
		int ubx = min(x+radius, 2147483647);
		int lby = max(y-radius, -2147483647);
		int uby = min(y+radius, 2147483647);
		rangex = queryx(lbx, ubx, 0, n-1, 0, n-1, vx, false, false);
		rangey = queryy(lby, uby, 0, n-1, 0, n-1, vy, false, false);
		//cout << "87" << endl;
		//cout << "rangex: " << rangex.first << " " << rangex.second << endl;
		//cout << "rangey: " << rangey.first << " " << rangey.second << endl;
		//cout << "_x: ";
		for(int i = rangex.first+1; i < rangex.second; i++) {
			if(visited[it.idx].find(vx[i].idx) == visited[it.idx].end()){
				//cout << vx[i].idx << ' ';
				_x.insert(vx[i].idx);
				//visited[it.idx][vx[i].idx] = 1;
				//visited[vx[i].idx][it.idx] = 1;
			}
		}
		//cout << endl;
		//cout << "_y: ";
		for(int i = rangey.first+1; i < rangey.second; i++){
			if(visited[it.idx].find(vy[i].idx) == visited[it.idx].end()){
				//cout << vy[i].idx << ' ';
				_y.insert(vy[i].idx);
				//visited[it.idx][vy[i].idx] = 1;
				//visited[vy[i].idx][it.idx] = 1;
			}
		}
		//cout << endl;
		set_intersection(_x.begin(), _x.end(), _y.begin(), _y.end(), inserter(intersection, intersection.begin()));
		int sz = intersection.size();
		ans += sz-1;
	}
	cout << ans/2 << endl;
}