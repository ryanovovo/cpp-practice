#include<bits/stdc++.h>
using namespace std;
#define ll long long int
int main(){
	set<ll> s;
	int n, l;
	cin >> n >> l;
	vector<pair<ll, ll>> v(n);
	for(int i = 0; i < n; i++){
		cin >> v[i].second >> v[i].first;
	}
	sort(v.begin(), v.end());
	s.insert(0);
	s.insert(l);
	ll ans = 0;
	for(int i = 0; i < n; i++){
		int cut = v[i].second;
		auto tmp = s.insert(cut);
		auto it = tmp.first;
		ans += *next(it) - *prev(it);
	}
	cout << ans << endl;

}