#include <bits/stdc++.h>
using namespace std;
class dose{
public:
	deque<int> r = {1, 2, 6, 5};
	deque<int> f = {1, 4, 6, 3};

	
	void move_right(){
		int ed = r.back();
		r.pop_back();
		r.push_front(ed);
		f[0] = r[0];
		f[2] = r[2];
	}
	void move_forward(){
		int ed = f.back();
		f.pop_back();
		f.push_front(ed);
		r[0] = f[0];
		r[2] = f[2];
	}

};

int main(){
	int n, m;
	cin >> n >> m;
	vector<dose> v(n+1);
	while(m--){
		int a, b;
		cin >> a >> b;
		if(b == -2){
			v[a].move_right();
		}
		else if(b == -1){
			v[a].move_forward();
		}
		else{
			swap(v[a], v[b]);
		}
	}
	for(int i = 1; i <= n; i++){
		cout << v[i].r.front() << ' ';
	}
	cout << endl;
}