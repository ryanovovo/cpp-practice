#include<iostream>
#include<vector>
#include<queue>
using namespace std;

#define MAX_INT 2147483647
struct edge{
	int u, v;
	int cap, rest;
};
vector<edge> edges;
vector<vector<int>>	G;
vector<int> level;
void add_edge(int u, int v, int cap){
	edges.push_back({u, v, cap, cap});
	edges.push_back({v, u, 0, 0});
	G[u].push_back(edges.size()-2);
	G[v].push_back(edges.size()-1);
}
vector<int> vis;
int dfs(int s, int t, int mn){
	if(s == t){
		return mn;
	}
	if(vis[s] == 1){
		return 0;
	}
	else{
		vis[s] = 1;
	}
	int flow = 0;
	for(auto x : G[s]){
		if(edges[x].rest > 0 && (level[edges[x].u]+1) == level[edges[x].v]){
			int f = dfs(edges[x].v, t, min(mn, edges[x].rest));
			if(f > 0){
				edges[x].rest -= f;
				edges[x^1].rest += f;
				mn -= f;
				flow += f;
				if(mn == 0) break;
			}
		}
	}
	return flow;
}
bool bfs(int s, int t){
	queue<int> q;
	q.push(s);
	int n = level.size();
	level.clear();
	level = vector<int>(n+1, 0);
	level[s] = 1;
	while(!q.empty()){
		int u = q.front();
		q.pop();
		for(auto x : G[u]){
			int v = edges[x].v;
			if(level[v] == 0 && edges[x].rest > 0){
				level[v] = level[u]+1;
				q.push(v);
			}
			else{
				continue;
			}
		}
	}
	return level[t] != 0;
}
void init(int n){
	edges.clear();
	G.clear();
	G = vector<vector<int>>(n+1);
	vis.clear();
	vis = vector<int>(n+1, 0);
	level.clear();
	level = vector<int>(n+1, 0);
}
/*int fk(int s, int t){
	int res = 0;
	int n = vis.size();
	while(true){
		vis.clear();
		vis = vector<int>(n+1, 0);
		int flow = dfs(s, t, MAX_INT);
		if(flow > 0){
			res += flow;
		}
		else{
			break;
		}
	}
	return res;
}*/
int dinic(int s, int t){
	int res = 0;
	int n = vis.size();
	while(bfs(s, t)){
		while(true){
			vis.clear();
			vis = vector<int>(n+1, 0);
			int flow = dfs(s, t, MAX_INT);
			if(flow > 0){
				res += flow;
			}
			else{
				break;
			}
		}
	}
	return res;
}
int main(){
	int n;
	int s, t, c;
	int no = 1;
	while(true){
		cin >> n;
		if(n == 0){
			break;
		}
		init(n);
		cin >> s >> t >> c;
		for(int i = 0; i < c; i++){
			int u, v, cap;
			cin >> u >> v >> cap;
			add_edge(u, v, cap);
			add_edge(v, u, cap);
		}
		int ans = dinic(s, t);
		cout << "Network " << no << endl;
		cout << "The bandwidth is " << ans << '.'<< endl;
		no++;
	}
}
