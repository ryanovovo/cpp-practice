#include <bits/stdc++.h>
using namespace std;
int n, cache, total_time, num_edges;
long long int mx = 0;
long long int ans = 0;
inline void dfs(vector<vector<int>> &graph, vector<vector<int>> &edges, int now, long long int val, int t){
	if(t == 0 || ans == mx || ans >= cache*t+val){
		return;
	}
	for(int v: edges[now]){
		long long int collected = min(cache, graph[now][v]);
		graph[now][v] -= collected;
		graph[v][now] -= collected;
		ans = max(ans, val+collected);
		dfs(graph, edges,  v, val+collected, t-1);
		graph[now][v] += collected;
		graph[v][now] += collected;
	}
	return;
}
int main(){
	ios_base::sync_with_stdio(0); cin.tie(0);
	cin >> n >> cache >> total_time >> num_edges;
	vector<vector<int>> graph(n+1, vector<int> (n+1, -1));
	vector<vector<int>> edges(n+1);
	for(int i = 0; i < num_edges; i++){
		int start, dest, garbage;
		cin >> start >> dest >> garbage;
		graph[start][dest] = garbage;
		graph[dest][start] = garbage;
		edges[start].push_back(dest);
		edges[dest].push_back(start);
		mx += garbage;
	}
	dfs(graph, edges, 1, 0, total_time);
	cout << ans << endl;
}