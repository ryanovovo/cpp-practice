#include <bits/stdc++.h>
using namespace std;
int ans = 0;
void solve(vector<int> &plants, const int &mx){
	for(int i = 1; i < plants.size(); i++){
		if(plants[i-1] - plants[i] > mx){
			ans += plants[i-1]-plants[i]-mx;
			plants[i] += plants[i-1]-plants[i]-mx;
		}
	}
}
int main(){
	vector<int> plants;
	int mx;
	int tmp;
	while(cin >> tmp){
		plants.push_back(tmp);
		char ch = getchar();
		if(ch == '\n'){
			break;
		}
	}
	cin >> mx;
	solve(plants, mx);
	reverse(plants.begin(), plants.end());
	solve(plants, mx);
	cout << ans << endl;
}