#include <bits/stdc++.h>
using namespace std;
int main(){
	int n;
	cin >> n;        
	vector<int> v(501, 0);
	for(int i = 0; i < n; i++){
		int val;
		cin >> val;
		v[val] += val;
	}
	vector<vector<int>> dp(501, vector<int>(2, 0));
	dp[1][1] = v[1];
	for(int i = 2; i <= 500; i++){
		dp[i][1] = v[i]+ max(dp[i-2][1], dp[i-2][0]);
		dp[i][0] = max(dp[i-1][1], dp[i-1][0]);
	}
	cout << max(dp[500][0], dp[500][1]) << endl;
}