#include <bits/stdc++.h>
using namespace std;
pair<int, int> solve(pair<int, int> &a, pair<int, int> &b){
	pair<int, int> res;
	if(a.first == 1 && b.first == 1){
		res.first = 0;
		res.second = max(a.second, b.second)+1;
	}
	else{
		res.first = 1;
		if(a.first == 0 && b.first == 0){
			res.second = min(a.second, b.second)+1;
		}
		else if(a.first == 0 && b.first == 1){
			res.second = a.second+1;
		}
		else if(a.first == 1 && b.first == 0){
			res.second = b.second+1;
		}
	}
	return res;
}
int main(){
	int x, y;
	cin >> x >> y;
	x--; y--;
	vector<vector<pair<int, int>>> dp(10000, vector<pair<int, int>>(3, {0, 0}));
	for(int i = 2; i <= x; i++){
		dp[i][1] = {1, 1};
	}
	for(int j = 2; j <= y; j++){
		dp[1][j%3] = {1, 1};
		dp[0][j%3] = {0, 0};
		for(int i = 2; i <= x; i++){
			dp[i][j%3] = solve(dp[i-1][(j%3) == 2 ? 0: (j%3)+3-2], dp[i-2][(j%3) == 0 ? 2 : (j%3)-1]);
		}
		/*for(int m = 0; m < 3; m++){
			for(int n = 0; n <= x; n++){
				cout << dp[n][m].second << ' ';
			}
			cout << endl;
		}
		cout << "==================" << endl;*/
	}
	if(dp[x][y%3].first == 1){
		cout << "Win " << dp[x][y%3].second << endl;
	}
	else{
		cout << "Lose " << dp[x][y%3].second << endl;
	}
}