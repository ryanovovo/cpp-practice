#include <bits/stdc++.h>
using namespace std;
int main(){
	int t;
	cin >> t;
	while(t--){
		long long int ans = 0;
		int n;
		cin >> n;
		vector<int> v(n);
		for(int i = 0; i < n; i++){
			cin >> v[i];
		}
		for(int i = 1; i < n; i++){
			long long int mx = max(v[i], v[i-1]);
			long long int mn = min(v[i], v[i-1]);
			ans = max(mx*mn, ans);
		}
		cout << ans << endl;
	}
}