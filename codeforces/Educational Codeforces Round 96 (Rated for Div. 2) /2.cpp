#include<bits/stdc++.h>
using namespace std;
int main(){
    int cases;
    cin >> cases;
    while(cases--){
        int n, k;
        cin >> n >> k;
        vector<int> arr(n, 0);
        for(int i = 0; i < n; i++){
            cin >> arr[i];
        }
        sort(arr.begin(), arr.end());
        reverse(arr.begin(), arr.end());
        long long int ans = 0;
        for(int i = 0; i <= k; i++){
            ans += arr[i];
        }
        cout << ans << endl;
    }
}
