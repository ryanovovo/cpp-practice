#include<bits/stdc++.h>
using namespace std;
int main(){
    int cases;
    cin >> cases;
    struct S {
        int x; //number of 3 
        int y; //5
        int z; //7
    };
    while(cases--){
        int rooms;
        cin >> rooms;
        vector<S> dp(rooms+8);
        for(int i = 0; i <= rooms; i++){
            dp[i].x = -1;
            dp[i].y = -1;
            dp[i].z = -1;
        }
        dp[0].x = 0;
        dp[0].y = 0;
        dp[0].z = 0;
        for(int i = 1; i <= rooms; i++){
            if(i-3 >= 0 && dp[i-3].x != -1){
                dp[i].x = dp[i-3].x+1;
                dp[i].y = dp[i-3].y;
                dp[i].z = dp[i-3].z;
            }
            else if(i-5 >= 0 && dp[i-5].x != -1){
                dp[i].x = dp[i-5].x;
                dp[i].y = dp[i-5].y+1;
                dp[i].z = dp[i-5].z;
            }
            else if(i-7 >= 0 && dp[i-7].x != -1){
                dp[i].x = dp[i-7].x;
                dp[i].y = dp[i-7].y;
                dp[i].z = dp[i-7].z+1;
            }
        }
        if(dp[rooms].x == -1){
            cout << "-1" << endl;
        }
        else{
            cout << dp[rooms].x << " " << dp[rooms].y << " " << dp[rooms].z << endl;
        }
    }
}
