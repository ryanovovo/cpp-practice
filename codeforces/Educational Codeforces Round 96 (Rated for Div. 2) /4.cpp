#include<bits/stdc++.h>
using namespace std;
int main(){
    int cases;
    cin >> cases;
    while(cases--){
        int n;
        cin >> n;
        string str;
        cin >> str;
        vector<int> dp(n, 0);
        dp[0] = 1;
        for(int i = 1; i < n; i++){
            if(i%2 == 0 && str[i] != str[i-1]){
                dp[i] = dp[i-1] + 1;
            }
            else{
                dp[i] = dp[i-1];
            }
        }
        cout << dp[n-1] << endl;
    }
}
