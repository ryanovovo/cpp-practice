#include<bits/stdc++.h>
using namespace std;
void buildSparseTable(vector<vector<int>> &sparseTable, vector<node> &tree, int root){
	int n = tree.size();
	for(int i = 0; i < n; i++){
		sparseTable[i][0] = i;
	}
	for(int i = 0; i < sparseTable.size(); i++){
		for(int k = 1; k < __lg(n)+1; k++){
			if((1<<k) <= tree[i].dep){
				sparseTable[i][k] = sparseTable[sparseTable[i][k-1]][k-1];
			}
			else{
				tree[i][k] = -1;
			}
		}
	}
	return;
}
void findDepth(vector<node> &tree, int root, int currentDepth){
	tree[root].depth = currentDepth;
	for(auto c : tree[root].ch){
		findDepth(tree, c, currentDepth+1);
	}
	return;
}
int findWeightSum(int u, vector<vector<int>> &sparseTable){
	
}
struct node{
	int anc;
	int dep;
	vector<int> ch;
};
int main(){
	int n, cost;
	cin >> n >> cost;
	int root;
	vector<node> tree(n);
	vector<vector<int>> sparseTable(n, vector<int>(__lg(n)+1, -1));
	for(int i = 0; i < n; i++){
		int boss;
		cin >> boss;
		if(boss == 0){
			root = i;
			tree[i].anc = -1;
		}
		else{
			tree[i].anc = boss;
			tree[boss].ch.push_back(i);
		}
	}


}