#include<bits/stdc++.h>
using namespace std;
int main(){
	int n;
	cin >> n;
	vector<pair<int, int>> v(n);
	for(int i = 0; i < n; i++){
		cin >> v[i].first >> v[i].second;
	}
	sort(v.begin(), v.end());
	int l = v[0].first;
	int r = v[0].second;
	int ans = r - l;
	for(int i = 1; i < n; i++){
		if(v[i].first >= r){
			l = v[i].first;
			r = v[i].second;
			ans += r - l;
		}
		else if(v[i].first < r && v[i].second > r){
			ans += v[i].second - r;
			r = v[i].second;
		}
	}
	cout << ans << endl;
}