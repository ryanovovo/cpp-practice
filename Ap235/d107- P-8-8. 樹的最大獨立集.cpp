#include<bits/stdc++.h>
using namespace std;
class node{
public:
	vector<int> ch;
	int anc;
};
int main(){
	int n;
	while(cin >> n){
		vector<node> tree(n);
		for(int i = 1; i < n; i++){
			int anc;
			cin >> anc;
			tree[i].anc = anc;
			tree[anc].ch.push_back(i);
		}
		vector<vector<int>> dp(n, vector<int>(2, 0));
		vector<int> is_done(n, 0);
		queue<int> q;
		for(int i = 0; i < n; i++){
			if(tree[i].ch.size() == 0){
				q.push(i);
			}
			dp[i][1] = 1;
		}
		while(!q.empty()){
			int root = q.front();
			int anc = tree[root].anc;
			// cout << root << endl;
			if(is_done[root] == tree[root].ch.size()){
				for(auto ch : tree[root].ch){
					dp[root][1] += dp[ch][0];
					dp[root][0] += max({dp[ch][0], dp[ch][1]});
					
				}
				is_done[anc]++;
				if(is_done[anc] == tree[anc].ch.size()){
					q.push(anc);
				}
			}
			q.pop();
		}
		cout << max(dp[0][0], dp[0][1]) << endl;
	}
}
