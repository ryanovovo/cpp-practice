#include<bits/stdc++.h>
using namespace std;

int main(){
	int n;
	cin >> n;
	vector<vector<pair<int, int>>> tree(n+1);
	vector<pair<int, int>> anc(n+1);
	vector<int> s(n+1);
	vector<int> w(n+1);
	vector<int> sz(n+1, 1);
	for(int i = 2; i <= n; i++){
		cin >> s[i];
	}
	for(int i = 2; i <= n; i++){
		cin >> w[i];
	}
	for(int i = 2; i <= n; i++){
		tree[s[i]].push_back({i, w[i]});
		anc[i] = {s[i], w[i]};
	}
	queue<int> q;
	for(int i = 2; i <= n; i++){
		if(tree[i].size() == 0){
			q.push(i);
		}
	}
	vector<int> is_done(n+1, 0);
	vector<int> dp(n+1, 0);
	vector<int> pre(n+1, 0);
	while(!q.empty()){
		int root = q.front();
		int a = anc[root].first;
		if(is_done[root] == tree[root].size()){
			sz[a] += sz[root];
			for(auto ch : tree[root]){
				int child_idx = ch.first;
				int weight = ch.second;
				dp[root] = dp[child_idx] + weight * sz[child_idx];
			}
			is_done[a]++;
			if(is_done[a] == tree[a].size()){
				q.push(a);
			}
		}
		q.pop();
	}
	q.push(1);
	while(!q.empty()){
		int root = q.front();
		int a = anc[root].first;
		int w = anc[root].second;
		pre[root] += (n-sz[anc]+1) * w;
		for(auto ch : tree[root]){
			int child_idx = ch.first
			w = ch.second
			pre[root] += dp[child_idx] + sz[child_idx]*()
		}
	}
	for(int i = 1; i <= n; i++){
		cout << dp[i] << ' ';
	}
	cout << endl;
}