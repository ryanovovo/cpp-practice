#include<bits/stdc++.h>
using namespace std;
int main(){
	int n;
	cin >> n;
	vector<int> v(n);
	for(int i = 0; i < n; i++){
		cin >> v[i];
	}
	vector<int> suf_max(n, 0);
	suf_max[n-1] = v[n-1];
	for(int i = n-2; i >= 0; i--){
		suf_max[i] = max(v[i], suf_max[i+1]);
	}
	int ans = 0;
	for(int i = 0; i < n; i++){
		ans = max(suf_max[i]-v[i], ans);
	}
	cout << ans << endl;
}