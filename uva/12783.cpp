#include<bits/stdc++.h>
using namespace std;
vector<vector<int>> edges;
vector<int> visited_node;
vector<int> times;
vector<int> low;
vector<vector<int>> tree_edge;
vector<vector<int>> back_edge;
vector<vector<int>> visited_edge;
vector<pair<int, int>> bridges;
set<pair<int, int>> check;
void dfs(int root, int t){
	visited_node[root] = 1;
	times[root] = t;
	low[root] = t;
	//cout << "times :" << times[root] << endl;
	for(int v : edges[root]){
		if(visited_node[v] == 0 && visited_edge[root][v] == 0){
			//cout << "tree_edge :" << v << endl;
			tree_edge[root].push_back(v);
			visited_node[v] = 1;
			visited_edge[root][v] = 1;
			visited_edge[v][root] = 1;
			dfs(v, t+1);
			low[root] = min(low[v], low[root]);
		}
		else if(visited_node[v] == 1 && visited_edge[root][v] == 0){
			//cout << "back_edge :" << v << endl;
			visited_edge[root][v] = 1;
			visited_edge[v][root] = 1;
			back_edge[root].push_back(v);
			low[root] = min(times[v], low[root]);
		}
	}
	return;
}
void find_bridge(int root){
	visited_node[root] = 1;
	for(int v : edges[root]){
		if(visited_node[v] == 0){
			//cout << v << endl;
			find_bridge(v);
			if(low[v] == times[v] && check.find({min(root, v), max(root, v)}) == check.end()){
				check.insert({min(root, v), max(root, v)});
				bridges.push_back({min(root, v), max(root, v)});
			}
		}
	}
}
int main(){
	int n, m;
	while(cin >> n >> m){
		if(n == 0 && m == 0){
			break;
		}
		edges.clear();
		visited_node.clear();
		visited_edge.clear();
		tree_edge.clear();
		back_edge.clear();
		times.clear();
		low.clear();
		bridges.clear();
		check.clear();
		for(int i = 0; i < n; i++){
			visited_node.push_back(0);
			times.push_back(0);
			low.push_back(0);
			visited_edge.push_back(vector<int>(1000, 0));
			edges.push_back(vector<int>());
			tree_edge.push_back(vector<int>());
			back_edge.push_back(vector<int>());
		}
		for(int i = 0; i < m; i++){
			int start, dest;
			cin >> start >> dest;
			edges[start].push_back(dest);
			edges[dest].push_back(start);
		}
		/*
		for(int i = 0; i < n; i++){
			for(int it : edges[i]){
				cout << i << ' ' << it << endl;
			}
		}*/
		dfs(0, 1);
		visited_node.clear();
		for(int i = 0; i < n;i++){
			visited_node.push_back(0);
		}
		/*for(int t = 0; t < n; t++){
			cout << t << " time " << times[t] << endl;
			cout << t << " low " << low[t] << endl;
		}*/
		find_bridge(0);
		sort(bridges.begin(), bridges.end());
		cout << bridges.size() << ' ';
		for(int i = 0; i < bridges.size(); i++){
			if(i != 0){
				cout << ' ';
			}
			cout << bridges[i].first << ' ' << bridges[i].second;
		}
		cout << endl;
	}
}
