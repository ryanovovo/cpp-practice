#include<bits/stdc++.h>
using namespace std;
#define maxInt 2147483647
struct node{
	vector<int> ch;
};
void findEularPath(int u, vector<pair<int, int>> &eularPath, vector<int> &in, vector<node> &tree, int currentLv, vector<int> &vis){
	in[u] = eularPath.size();
	eularPath.push_back({currentLv, u});
	vis[u] = 1;
	for(auto c : tree[u].ch){
		if(vis[c] == 0){
			findEularPath(c, eularPath, in, tree, currentLv+1, vis);
			eularPath.push_back({currentLv, u});
			vis[c] = 1;
		}
	}
	return;
}
int queryMin(int l, int r, vector<pair<int, int>> &eularPath, vector<vector<int>> &sparseTable){
	if(l > r){
		swap(l, r);
	}
	int len = r - l + 1;
	int k = __lg(len);
	//cout <<" l: " << l <<" k: "<< k << endl;
	//cout << sparseTable[l][k] << endl;
	if(eularPath[sparseTable[l][k]].first < eularPath[sparseTable[r-(1<<k)+1][k]].first){
		return sparseTable[l][k];
	}
	else{
		return sparseTable[r-(1<<k)+1][k];
	}
}
void buildSparseTable(vector<pair<int, int>> &eularPath, vector<vector<int>> &sparseTable){
	for(unsigned int i = 0; i < eularPath.size(); i++){
		sparseTable[i][0] = i;
		//cout << sparseTable[i][0]<< endl;
	}
	int len = eularPath.size();
	for(int k = 1; k < __lg(len)+1; k++){
		for(int i = 0; i < len; i++){
			if(i + (1<<k) > len){
				break;
			}
			else{
				if(eularPath[sparseTable[i][k-1]].first < eularPath[sparseTable[i+(1<<(k-1))][k-1]].first){
					sparseTable[i][k] = sparseTable[i][k-1];
				}
				else{
					sparseTable[i][k] = sparseTable[i+(1<<(k-1))][k-1];
				}
				//cout << "i: " << i << " k: "<< k << ' ' <<eularPath[sparseTable[i][k]].first << ' ';
			}
			//cout << endl;
		}
	}
	return;
}
int main(){
	int n, q;
	cin >> n >> q;
	vector<node> tree(n+1);
	vector<pair<int, int>> eularPath; // (lv, idx)
	vector<int> in(n+1);
	for(int i = 0;i < n-1; i++){
		int a, b;
		cin >> a >> b;
		tree[a].ch.push_back(b);
		tree[b].ch.push_back(a);
	}
	vector<int> vis(n+1, 0);
	findEularPath(1, eularPath, in, tree, 1, vis);
	//cout << endl;
	int len = eularPath.size();
	vector<vector<int>> sparseTable(len+1, vector<int>(__lg(len)+1, maxInt));
	buildSparseTable(eularPath, sparseTable); 
	//cout << sparseTable[1][2] << endl;
	while(q--){
		int v1, v2;
		cin >> v1 >> v2;
		//cout << "node1: "<< in[v1] << ' ' << "node2: "<< in[v2] << endl;
		cout << eularPath[queryMin(in[v1], in[v2], eularPath, sparseTable)].second << endl;
	}
}
