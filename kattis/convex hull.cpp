#include<bits/stdc++.h>
using namespace std;
typedef int Double;
struct Point {
	Double x,y;
	bool operator < (const Point &b)const{    // 比較
		return tie(x,y) < tie(b.x,b.y);
	    //return atan2(y,x) < atan2(b.y,b.x);
	}
	Point operator + (const Point &b)const{   // 向量加法
	    return {x+b.x,y+b.y};
	}
	Point operator - (const Point &b)const{   // 向量減法
	    return {x-b.x,y-b.y};
	}
	Point operator * (const Double &d)const{  // 向量伸縮
	    return {d*x, d*y};
	}
	Double operator * (const Point &b)const{  // 向量內積
		return x*b.x + y*b.y;
	}
	Double operator % (const Point &b)const{  // 向量外積
	    return x*b.y - y*b.x;
	}

};
vector<Point> solve(vector<Point> &p){
	sort(p.begin(), p.end(), cmp);
	int m = 0;
	int n = p.size();
	vector<Point> res
	for(int i = 0; i < n; i++){
		while(m > 1 && (res[m-1]-res[m-2]) % (p[i]-res[m-2]) <= 0) m--;
		res[m++] = p[i];
	}
	int k = m;
	for(int i = n-2; i >= 0; i--){
		while(m > k && (res[m-1]-res[m-2]) % (p[i]-res[m-2]) <= 0) m--;
		res[m++] = p[i];
	}
	if(n > 1) m--;
	return res;
}
int main(){
	int n;
	while(cin >> n){
		if(n == 0) break;
		vector<Point> points(n);
		for(int i = 0; i < n; i++){
			cin >> points[i].x;
			cin >> points[i].y;
		}
		sort(points.begin(), points.end(), cmp);
		vector<Point> res = solve(points);
		cout << res.size() << endl;
		for(auto p : res){
			cout << p.x << ' ' << p.y << endl;
		}
	}
}