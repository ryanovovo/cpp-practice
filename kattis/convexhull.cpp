#include<bits/stdc++.h>
using namespace std;
typedef double Double;
struct Point {
  Double x,y;

  bool operator < (const Point &b)const{    // 比較
    return tie(x,y) < tie(b.x,b.y);
    //return atan2(y,x) < atan2(b.y,b.x);
  }
  Point operator + (const Point &b)const{   // 向量加法
    return {x+b.x,y+b.y};
  }
  Point operator - (const Point &b)const{   // 向量減法
    return {x-b.x,y-b.y};
  }
  Point operator * (const Double &d)const{  // 向量伸縮
    return {d*x, d*y};
  }
  Double operator * (const Point &b)const{  // 向量內積
    return x*b.x + y*b.y;
  }
  Double operator % (const Point &b)const{  // 向量外積
    return x*b.y - y*b.x;
  }
};
vector<Point> convexhull(vector<Point> &p){
	sort(p.begin(), p.end());
	int n = p.size();
	int m = 0;
	vector<Point> res;
	for(int i = 0; i < n; i++){
		while(m >= 2 && (p[i]-res[m-2]) % (res[m-1]-res[m-2]) < 0){
			res.pop_back();
			m--;
		}
		res.push_back(p[i]);
		m++;
	}
	for(int i = n-2; i >= 0; i--){
		while(m >= 2 && (p[i]-res[m-2]) % (res[m-1]-res[m-2]) < 0){
			res.pop_back();
			m--;
		}
		res.push_back(p[i]);
		m++;
	}
	return res;
}
int main(){

}