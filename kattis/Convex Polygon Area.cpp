#include<bits/stdc++.h>
using namespace std;
typedef double Double;
struct Point {
  	Double x,y;
  	bool operator < (const Point &b)const{    // 比較
    	return tie(x,y) < tie(b.x,b.y);
    	//return atan2(y,x) < atan2(b.y,b.x);
  	}
  	Point operator + (const Point &b)const{   // 向量加法
    	return {x+b.x,y+b.y};
  	}
  	Point operator - (const Point &b)const{   // 向量減法
    	return {x-b.x,y-b.y};
  	}
  	Point operator * (const Double &d)const{  // 向量伸縮
    	return {d*x, d*y};
  	}
  	Double operator * (const Point &b)const{  // 向量內積
    	return x*b.x + y*b.y;
  	}
  	Double operator % (const Point &b)const{  // 向量外積
    	return x*b.y - y*b.x;
  	}
};
vector<Point> convexhull(vector<Point> &p){ // 輸入點，回傳凸包
	sort(p.begin(), p.end());
	int n = p.size();
	int m = 0;
	vector<Point> res;
	for(int i = 0; i < n; i++){
		while(m >= 2 && (p[i]-res[m-2]) % (res[m-1]-res[m-2]) < 0){
			res.pop_back();
			m--;
		}
		res.push_back(p[i]);
		m++;
	}
	for(int i = n-2; i >= 0; i--){
		while(m >= 2 && (p[i]-res[m-2]) % (res[m-1]-res[m-2]) < 0){
			res.pop_back();
			m--;
		}
		res.push_back(p[i]);
		m++;
	}
	return res;
}
double _2d_area(vector<Point> &poly){ // 輸入簡單多邊形（起點和終點須重複, 已排序好） 回傳間單多邊形的面積
	int n = poly.size();
	double res = 0.0;
	for(int i = 0; i < n-1; i++){
		res += (poly[i] % poly[i+1]);
	}
	return abs(res/2);
}
int main(){
	int cases;
	cin >> cases;
	while(cases--){
		int n;
		cin >> n;
		vector<Point> p(n);
		for(int i = 0; i < n; i++){
			cin >> p[i].x >> p[i].y;
		}
		p.push_back(p[0]);
		double ans = _2d_area(p);
		cout << fixed << setprecision(1) << ans << endl;
	}
}