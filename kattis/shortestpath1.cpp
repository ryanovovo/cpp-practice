#include<bits/stdc++.h>
using namespace std;
int main(){
	int nodes, edges, queries, s;
	while(cin >> nodes >> edges >> queries >> s){
		if(nodes == 0 && edges == 0 && queries == 0 && s == 0){
			break;
		}
		vector<int> weight(nodes, INT_MAX);
		weight[s] = 0;
		vector<vector<pair<int, int>>> graph(nodes); //st dest nd cost
		for(int i = 0; i < edges ; i++){
			int start, dest, cost;
			cin >> start >> dest >> cost;
			graph[start].push_back({dest, cost});
		}
		priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;// st weight nd idx
		pq.push({0, s});
		while(!pq.empty()){
			int start = pq.top().second;
			pq.pop();
			for(auto dest : graph[start]){
				int d = dest.first;
				int cost = dest.second;
				if(weight[d] > weight[start] + cost){
					weight[d] = weight[start] + cost;
					pq.push({weight[d], d});
				}
			}
		}
		for(int i = 0; i < queries; i++){
			int n;
			cin >> n;
			if(weight[n] == INT_MAX){
				cout << "Impossible" << endl;
			}
			else{
				cout << weight[n] << endl;
			}
		}
	}
}
